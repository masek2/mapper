<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use JSONAPI\Mapper\Annotation as API;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Schema\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;

/**
 * Class MetaExample
 *
 * @package JSONAPI\Test
 */
#[API\Resource("meta")]
#[API\Meta("getMeta")]
class MetaExample implements Resource
{
    /**
     * @var string
     */
    private string $id;

    /**
     * MetaExample constructor.
     *
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    #[API\Id]
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Meta
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     */
    public function getMeta(): Meta
    {
        return new Meta(
            [
                'for' => MetaExample::class
            ]
        );
    }

    /**
     * @return DummyRelation
     */
    #[API\Relationship(DummyRelation::class)]
    #[API\Meta("getRelationMeta")]
    public function getRelation(): DummyRelation
    {
        return new DummyRelation('relation1');
    }

    /**
     * @return Meta
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     */
    public function getRelationMeta(): Meta
    {
        return new Meta(
            [
                'for' => DummyRelation::class
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            Id::createByMethod('getId'),
            'meta',
            [],
            [
                Relationship::createByMethod(
                    'getRelation',
                    DummyRelation::class,
                    null,
                    null,
                    false,
                    \JSONAPI\Mapper\Metadata\Meta::create('getRelationMeta')
                )
            ],
            \JSONAPI\Mapper\Metadata\Meta::create('getMeta')
        );
    }
}

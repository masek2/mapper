<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Valid;

use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Schema\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;
use JSONAPI\Mapper\Annotation as API;

/**
 * Class ThirdLevel
 *
 * @package JSONAPI\Test\Resources\Valid
 */
#[API\Resource("third")]
class ThirdLevel implements Resource
{
    /**
     * @var string
     */
    #[API\Id]
    public string $id;

    /**
     * @var string|null
     */
    #[API\Attribute]
    public ?string $property = null;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            Id::createByProperty('id'),
            'third',
            [Attribute::createByProperty('property')],
        );
    }
}

<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Resources\Invalid;

use JSONAPI\Mapper\Annotation as API;

/**
 * Class WithBadMethodSignature
 *
 * @package JSONAPI\Test\Resources\Invalid
 */
#[API\Resource("bad-signature")]
class WithBadMethodSignature
{
    private $property;

    /**
     * @return mixed
     */
    #[API\Attribute]
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param mixed $property
     * @param       $anotherArgument
     */
    public function setProperty($property, $anotherArgument): void
    {
        $this->property = $property;
    }
}

<?php


namespace JSONAPI\Mapper\Test\Resources\Invalid;


use JSONAPI\Mapper\Annotation as API;
use JSONAPI\Mapper\Encoding\ObjectCollection;

/**
 * Class BadRelationshipGetter
 *
 * @package JSONAPI\Test\Resources\Invalid
 */
#[API\Resource("bad-relationship-getter")]
class BadRelationshipGetter
{

    /**
     * @return ObjectCollection
     */
    #[API\Relationship("SomeClass")]
    public function getRelation()
    {
        return new ObjectCollection();
    }
}

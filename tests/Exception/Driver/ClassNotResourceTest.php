<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Exception\Driver;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Exception\Driver\ClassNotResource;
use JSONAPI\Mapper\Test\Resources\Invalid\NotResource;
use PHPUnit\Framework\TestCase;

class ClassNotResourceTest extends TestCase
{
    public function testConstruct()
    {
        $e = new ClassNotResource('MyClass');
        $this->assertInstanceOf(ClassNotResource::class, $e);
        $this->assertStringContainsString('MyClass', $e->getMessage());
    }

    public function testUsage()
    {
        $this->expectException(ClassNotResource::class);
        $driver = new AnnotationDriver();
        $driver->getClassMetadata(get_class(new NotResource()));
    }
}

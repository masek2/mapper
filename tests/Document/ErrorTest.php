<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Mapper\Document\Error;
use JSONAPI\Mapper\Document\Error\DefaultErrorFactory;
use JSONAPI\Mapper\Document\Error\ErrorFactory;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Meta;
use JSONAPI\Mapper\Exception\InvalidArgumentException;
use JSONAPI\Mapper\URI\QueryPartInterface;
use PHPUnit\Framework\TestCase;
use Swaggest\JsonSchema\InvalidValue;

/**
 * Class ErrorTest
 *
 * @package JSONAPI\Test\Document
 */
class ErrorTest extends TestCase
{
    public function testConstruct()
    {
        $instance = new DefaultErrorFactory();
        $this->assertInstanceOf(ErrorFactory::class, $instance);
    }

    public function testFromThrowable()
    {
        $i = new DefaultErrorFactory();
        $error = $i->fromThrowable(new \Exception("Unknown exception"));
        $this->assertInstanceOf(Error::class, $error);
        $jae = $i->fromThrowable(new InvalidArgumentException());
        $this->assertInstanceOf(Error::class, $jae);
        $se = $i->fromThrowable(new InvalidValue());
        $this->assertInstanceOf(Error::class, $se);
    }

    public function testSetters()
    {
        $source = Error\Source::parameter(QueryPartInterface::SORT_PART_KEY);
        $error = new Error();
        $error->setMeta(new Meta(['custom' => 'property']));
        $error->setId('id');
        $error->setStatus(StatusCodeInterface::STATUS_OK);
        $error->setDetail('detail');
        $error->setTitle('title');
        $error->setCode('code');
        $error->setSource($source);
        $error->getLinks()->add(new Link('about', 'http://about.error.com'));
        $json = $error->jsonSerialize();

        $this->assertObjectHasAttribute('id', $json);
        $this->assertEquals('id', $json->id);
        $this->assertObjectHasAttribute('status', $json);
        $this->assertEquals(200, $json->status);
        $this->assertObjectHasAttribute('detail', $json);
        $this->assertEquals('detail', $json->detail);
        $this->assertObjectHasAttribute('title', $json);
        $this->assertEquals('title', $json->title);
        $this->assertObjectHasAttribute('code', $json);
        $this->assertEquals('code', $json->code);
        $this->assertObjectHasAttribute('source', $json);
        $this->assertObjectHasAttribute('meta', $json);
        $this->assertObjectHasAttribute('links', $json);
    }
}

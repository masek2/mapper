<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Document\Builder;
use JSONAPI\Mapper\Document\BuilderFactory;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class BuilderFactoryTest extends TestCase
{
    public static MetadataRepository $metadata;
    /**
     * @var Configuration configuration
     */
    private static Configuration $configuration;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass(); // TODO: Change the autogenerated stub
        self::$metadata      = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$configuration = new Configuration(self::$metadata, 'http://unit.test.org');
    }

    public function testNew()
    {
        $request  = ServerRequestFactory::createFromGlobals();
        $factory  = new BuilderFactory(self::$configuration);
        $composer = $factory->create($request);
        $this->assertInstanceOf(Builder::class, $composer);
    }
}

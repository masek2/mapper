<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Document;

use JSONAPI\Mapper\Document\Attribute;
use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Exception\Document\AlreadyInUse;
use PHPUnit\Framework\TestCase;

class ResourceObjectTest extends TestCase
{
    public function testAlreadyInUse()
    {
        $this->expectException(AlreadyInUse::class);
        $o = new ResourceObject(new Type('type'), new Id('id'));
        $o->addAttribute(new Attribute('type', 'data'));
    }
}

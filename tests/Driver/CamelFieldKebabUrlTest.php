<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Driver;

use JSONAPI\Mapper\Driver\CamelFieldKebabUrl;
use PHPUnit\Framework\TestCase;

class CamelFieldKebabUrlTest extends TestCase
{
    private string $className = 'TestResource';
    private string $typeField = 'test-resources';
    private string $typeURL = 'test-resources';
    private string $relationshipName = 'SomeRelation';
    private string $relationshipField = 'someRelation';
    private string $relationshipURL = 'some-relation';

    public function testTypeFieldToURL()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->typeField, $strategy->typeFieldToURL($this->typeURL));
    }

    public function testRelationshipURLToField()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->relationshipField, $strategy->relationshipURLToField($this->relationshipURL));
    }

    public function testRelationshipFieldToURL()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->relationshipURL, $strategy->relationshipFieldToURL($this->relationshipField));
    }

    public function testTypeClassNameToField()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->typeField, $strategy->typeClassNameToField($this->className));
    }

    public function testReflectionNameToField()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->relationshipField, $strategy->reflectionNameToField($this->relationshipName));
    }

    public function testTypeURLToField()
    {
        $strategy = new CamelFieldKebabUrl();
        $this->assertEquals($this->typeField, $strategy->typeURLToField($this->typeURL));
    }
}

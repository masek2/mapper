<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Metadata;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\AttributesProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\LinksProcessor;
use JSONAPI\Mapper\Encoding\MetaProcessor;
use JSONAPI\Mapper\Encoding\RelationshipsProcessor;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use JSONAPI\Mapper\Test\Resources\Valid\MetaExample;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Parser;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * Class EncoderTest
 *
 * @package JSONAPI\Test\Metadata
 */
class EncoderTest extends TestCase
{
    public function testConstruct()
    {
        $metadata      = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $baseURL       = 'http://unit.test.org';
        $configuration = new Configuration($metadata, $baseURL);
        $request       = ServerRequestFactory::createFromGlobals();
        $uri           = (new Parser($configuration))->parse($request);
        $logger        = new NullLogger();
        $encoder       = new Encoder($metadata);
        $linkFactory   = new LinkFactory($metadata);
        $encoder->addProcessor(new AttributesProcessor($metadata, $logger, $uri->getFieldset()));
        $encoder->addProcessor(new RelationshipsProcessor($encoder, $metadata, $logger));
        $encoder->addProcessor(new LinksProcessor($linkFactory, 'http://unit.test.org'));
        $encoder->addProcessor(new MetaProcessor($metadata, $logger));
        $this->assertInstanceOf(Encoder::class, $encoder);
        return $encoder;
    }

    /**
     * @depends testConstruct
     */
    public function testIdentify(Encoder $encoder)
    {
        $object     = new GettersExample('id');
        $identifier = $encoder->identify($object);
        $this->assertInstanceOf(ResourceObjectIdentifier::class, $identifier);
    }

    /**
     * @depends testConstruct
     */
    public function testEncode(Encoder $encoder)
    {
        $object   = new GettersExample('id');
        $resource = $encoder->encode($object);
        $this->assertInstanceOf(ResourceObject::class, $resource);
    }

    /**
     * @depends testConstruct
     */
    public function testRelationshipMetaEncode(Encoder $encoder)
    {
        $object   = new MetaExample('meta');
        $resource = $encoder->encode($object);
        $this->assertInstanceOf(ResourceObject::class, $resource);
        $this->assertFalse($resource->jsonSerialize()->relationships->relation->getMeta()->isEmpty());
        $this->assertEquals(
            '{"type":"meta","id":"meta","meta":{"for":"JSONAPI\\\Mapper\\\Test\\\Resources\\\Valid\\\MetaExample"},"relationships":{"relation":{"data":{"type":"relation","id":"relation1"},"links":{"self":"http:\/\/unit.test.org\/meta\/meta\/relationships\/relation","related":"http:\/\/unit.test.org\/meta\/meta\/relation"},"meta":{"for":"JSONAPI\\\Mapper\\\Test\\\Resources\\\Valid\\\DummyRelation"}}},"links":{"self":"http:\/\/unit.test.org\/meta\/meta"}}',
            json_encode($resource)
        );
    }
}

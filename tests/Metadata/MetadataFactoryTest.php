<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Metadata;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * Class MetadataFactoryTest
 *
 * @package JSONAPI\Test\Metadata
 */
class MetadataFactoryTest extends TestCase
{
    public function testCreate()
    {
        $repository = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        $this->assertInstanceOf(MetadataRepository::class, $repository);
        $repository = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
        $this->assertInstanceOf(MetadataRepository::class, $repository);
    }
}

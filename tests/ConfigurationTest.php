<?php

/**
 * Created by lasicka@logio.cz
 * at 11.10.2021 10:48
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Driver\CamelFieldKebabUrl;
use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Exception\InvalidConfigurationParameter;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\URI\Fieldset\FieldsetParser;
use JSONAPI\Mapper\URI\Filtering\OData\ExpressionFilterParser;
use JSONAPI\Mapper\URI\Inclusion\InclusionParser;
use JSONAPI\Mapper\URI\Pagination\LimitOffsetPagination;
use JSONAPI\Mapper\URI\Path\PathParser;
use JSONAPI\Mapper\URI\Sorting\SortParser;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class ConfigurationTest extends TestCase
{
    /**
     * @var MetadataRepository mr
     */
    private static MetadataRepository $mr;
    /**
     * @var string url
     */
    private static string $url;

    public static function setupBeforeClass(): void
    {
        self::$mr  = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
        self::$url = 'http://unit.test.org';
    }


    public function testConstruct()
    {
        $min  = new Configuration(self::$mr, self::$url);
        $pp = new PathParser(self::$mr, self::$url);
        $full = new Configuration(
            self::$mr,
            self::$url,
            false,
            false,
            false,
            new FieldsetParser(),
            new ExpressionFilterParser(self::$mr),
            new InclusionParser(),
            new LimitOffsetPagination(),
            $pp,
            new SortParser(),
            new CamelFieldKebabUrl(),
            new NullLogger()
        );
        $this->assertInstanceOf(Configuration::class, $min);
        $this->assertInstanceOf(Configuration::class, $full);
    }

    public function badUrlProvider()
    {
        return [
            ['unit.test.org'],
            [''],
            ['unit@test.com']
        ];
    }

    /**
     * @dataProvider badUrlProvider
     */
    public function testInvalidURL($url)
    {
        $this->expectException(InvalidConfigurationParameter::class);
        new Configuration(self::$mr, $url);
    }
}

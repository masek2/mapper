<?php

/**
 * Created by tomas
 * at 20.03.2021 22:20
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Encoding;

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\AttributesProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\LinksProcessor;
use JSONAPI\Mapper\Encoding\MetaProcessor;
use JSONAPI\Mapper\Encoding\RelationshipsProcessor;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\URI\Fieldset\FieldsetParser;
use JSONAPI\Mapper\URI\Inclusion\InclusionInterface;
use JSONAPI\Mapper\URI\Inclusion\InclusionParser;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Pagination\LimitOffsetPagination;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class EncoderTest extends TestCase
{
    private static MetadataRepository $metadata;
    private static FieldsetInterface $fieldset;
    private static InclusionInterface $inclusion;
    private static LinkFactory $linkFactory;
    private static LoggerInterface $logger;
    private static LimitOffsetPagination $pagination;

    public static function setUpBeforeClass(): void
    {
        self::$metadata    = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$fieldset    = (new FieldsetParser())->parse([]);
        self::$inclusion   = (new InclusionParser())->parse('');
        self::$linkFactory = new LinkFactory(self::$metadata);
        self::$logger      = new NullLogger();
        self::$pagination  = new LimitOffsetPagination();
    }

    public function testIdentify()
    {
        $encoder = new Encoder(self::$metadata, self::$logger);
        $encoder->addProcessor(new AttributesProcessor(self::$metadata, self::$logger, self::$fieldset));
        $encoder->addProcessor(
            new RelationshipsProcessor(
                $encoder,
                self::$metadata,
                self::$logger,
                self::$inclusion,
                self::$fieldset,
                true,
                25
            )
        );
        $encoder->addProcessor(new MetaProcessor(self::$metadata, self::$logger));
        $encoder->addProcessor(new LinksProcessor(self::$linkFactory, 'http://unit.test.org/api'));
        $object   = new GettersExample('id');
        $resource = $encoder->identify($object);
        $result   = json_encode($resource);
        $check    = '{"type":"getter","id":"id"}';
        $this->assertEquals($check, $result);
    }

    public function testEncode()
    {
        $encoder = new Encoder(self::$metadata, self::$logger);
        $encoder->addProcessor(new AttributesProcessor(self::$metadata, self::$logger, self::$fieldset));
        $encoder->addProcessor(
            new RelationshipsProcessor(
                $encoder,
                self::$metadata,
                self::$logger,
                self::$inclusion,
                self::$fieldset,
                true,
                25
            )
        );
        $encoder->addProcessor(new MetaProcessor(self::$metadata, self::$logger));
        $encoder->addProcessor(new LinksProcessor(self::$linkFactory, 'http://unit.test.org/api', self::$pagination));
        $object   = new GettersExample('id');
        $resource = $encoder->encode($object);
        $result   = json_encode($resource);
        $check    = '{"type":"getter","id":"id","attributes":{"stringProperty":"string value","intProperty":1,"doubleProperty":0.1,"arrayProperty":[1,2,3],"boolProperty":true,"dtoProperty":{"word":"string-value","number":1234,"boolean":true},"dateProperty":"2022-01-01T00:00:00+02:00"},"relationships":{"relation":{"data":{"type":"relation","id":"relation1"},"links":{"self":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/relation","related":"http:\/\/unit.test.org\/api\/getter\/id\/relation"}},"collection":{"links":{"self":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/collection","related":"http:\/\/unit.test.org\/api\/getter\/id\/collection"}},"doctrineCollection":{"data":[{"type":"relation","id":"relation2"},{"type":"relation","id":"relation3"},{"type":"relation","id":"relation4"},{"type":"relation","id":"relation5"},{"type":"relation","id":"relation6"}],"links":{"self":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/doctrine-collection","related":"http:\/\/unit.test.org\/api\/getter\/id\/doctrine-collection","first":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/doctrine-collection?page%5Boffset%5D=0&page%5Blimit%5D=5","next":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/doctrine-collection?page%5Boffset%5D=5&page%5Blimit%5D=5","last":"http:\/\/unit.test.org\/api\/getter\/id\/relationships\/doctrine-collection?page%5Boffset%5D=5&page%5Blimit%5D=5"}}},"links":{"self":"http:\/\/unit.test.org\/api\/getter\/id"}}';
        $this->assertEquals($check, $result);
    }
}

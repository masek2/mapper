<?php

/**
 * Created by lasicka@logio.cz
 * at 24.05.2021 15:04
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\Middleware;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Exception\Http\UnexpectedFieldDataType;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Middleware\DocumentParser;
use JSONAPI\Mapper\URI\Path\PathInterface;
use JSONAPI\Mapper\URI\Path\PathParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class DocumentParserTest extends TestCase
{
    public static MetadataRepository $metadata;
    public static PathInterface $path;

    public static function setUpBeforeClass(): void
    {
        self::$metadata = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$path     = (new PathParser(self::$metadata, 'http://unit.test.org'))
            ->parse('/getter/1', RequestMethodInterface::METHOD_GET);
    }

    /**
     * @dataProvider good
     */
    public function testDecode($object)
    {
        $factory = new DocumentParser(self::$metadata, self::$path);
        $data    = json_encode($object);
        $doc     = $factory->decode($data);
        $this->assertInstanceOf(Document::class, $doc);
    }

    /**
     * @dataProvider bad
     */
    public function testErrors($object)
    {
        $this->expectException(UnexpectedFieldDataType::class);
        $factory = new DocumentParser(self::$metadata, self::$path);
        $data    = json_encode($object);
        $factory->decode($data);
    }

    public function good(): array
    {
        return [
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => "string",
                            "intProperty"    => 1,
                            "doubleProperty" => 1.0,
                            "boolProperty"   => true
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => null,
                            "intProperty"    => 0,
                            "doubleProperty" => 0.0,
                            "boolProperty"   => false
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => null,
                            "intProperty"    => 1e1,
                            "doubleProperty" => 1e-1,
                            "boolProperty"   => false
                        ]
                    ]
                ]
            ]
        ];
    }

    public function bad(): array
    {
        return [
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => 1,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => false,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => 1.0,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "stringProperty" => [],
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "intProperty" => .1,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "intProperty" => false,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "intProperty" => "1",
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "intProperty" => null,
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "doubleProperty" => "1.1",
                        ]
                    ]
                ]
            ],
            [
                [
                    "data" => [
                        "type"       => "getter",
                        "id"         => "1",
                        "attributes" => [
                            "doubleProperty" => false,
                        ]
                    ]
                ]
            ]
        ];
    }
}

<?php

/**
 * Created by tomas
 * 10.07.2022 14:59
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\URI;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Test\Resources\Valid\GettersExample;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Parser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class LinkFactoryTest extends TestCase
{
    /**
     * @var MetadataRepository mr
     */
    private static MetadataRepository $mr;
    /**
     * @var string url
     */
    private static string $baseURL;
    private static Configuration $configuration;

    public static function setUpBeforeClass(): void
    {
        self::$mr            = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new AnnotationDriver()
        );
        self::$baseURL       = 'http://unit.test.org';
        self::$configuration = new Configuration(self::$mr, self::$baseURL);
    }

    public function testConstruct(): LinkFactory
    {
        $instance = new LinkFactory(self::$mr);
        $this->assertInstanceOf(LinkFactory::class, $instance);
        return $instance;
    }

    /**
     * @depends testConstruct
     */
    public function testGetDocumentLinks(LinkFactory $factory)
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up      = (new Parser(self::$configuration))->parse($request);
        $links   = $factory->getDocumentLinks($up);
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertEquals(
            "/getter/uuid?include=collection&fields%5Bresource%5D=publicProperty,privateProperty,relations",
            $links[Link::SELF]
        );
    }

    /**
     * @depends testConstruct
     */
    public function testGetResourceLinksByType(LinkFactory $factory)
    {
        $links = $factory->getResourceLinksByType('getter');
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertEquals('/getter', $links['all']);
        $this->assertEquals("/getter/{id}", $links[Link::SELF]);
        $this->assertEquals("/getter/{id}/relationships/relation", $links['relation'][Link::SELF]);
        $this->assertEquals("/getter/{id}/relation", $links['relation'][Link::RELATED]);
    }

    /**
     * @depends testConstruct
     */
    public function testGetResourceLinks(LinkFactory $factory)
    {
        $meta  = self::$mr->getByType('getter');
        $links = $factory->getResourceLinks($meta);
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertEquals('/getter', $links['all']);
        $this->assertEquals("/getter/{id}", $links[Link::SELF]);
        $this->assertEquals("/getter/{id}/relationships/relation", $links['relation'][Link::SELF]);
        $this->assertEquals("/getter/{id}/relation", $links['relation'][Link::RELATED]);
    }

    /**
     * @depends testConstruct
     */
    public function testGetResourceLinksByClass(LinkFactory $factory)
    {
        $links = $factory->getResourceLinksByClass(GettersExample::class);
        $this->assertArrayHasKey(Link::SELF, $links);
        $this->assertEquals('/getter', $links['all']);
        $this->assertEquals("/getter/{id}", $links[Link::SELF]);
        $this->assertEquals("/getter/{id}/relationships/relation", $links['relation'][Link::SELF]);
        $this->assertEquals("/getter/{id}/relation", $links['relation'][Link::RELATED]);
    }
}

<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Test\URI;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\URI\Filtering\FilterInterface;
use JSONAPI\Mapper\URI\Inclusion\InclusionInterface;
use JSONAPI\Mapper\URI\Pagination\PaginationInterface;
use JSONAPI\Mapper\URI\Path\PathInterface;
use JSONAPI\Mapper\URI\Sorting\SortInterface;
use JSONAPI\Mapper\URI\Parser;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

class ParserTest extends TestCase
{
    private static MetadataRepository $mr;
    private static string $baseURL;
    /**
     * @var Configuration configuration
     */
    private static Configuration $configuration;

    public static function setUpBeforeClass(): void
    {
        self::$mr = MetadataFactory::create(
            [RESOURCES . '/valid'],
            new Psr16Cache(new ArrayAdapter()),
            new SchemaDriver()
        );
        self::$baseURL = 'http://unit.test.org';
        self::$configuration = new Configuration(self::$mr, self::$baseURL);
    }

    public function testGetFieldset()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(FieldsetInterface::class, $up->getFieldset());
    }

    public function testGetFilter()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(FilterInterface::class, $up->getFilter());
    }

    public function testGetSort()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(SortInterface::class, $up->getSort());
    }

    public function testConstruct()
    {
        $up = new Parser(self::$configuration);
        $this->assertInstanceOf(Parser::class, $up);
    }

    public function testGetInclusion()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(InclusionInterface::class, $up->getInclusion());
    }

    public function testGetPath()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(PathInterface::class, $up->getPath());
    }

    public function testGetPagination()
    {
        $request = ServerRequestFactory::createFromGlobals();
        $up = (new Parser(self::$configuration))->parse($request);
        $this->assertInstanceOf(PaginationInterface::class, $up->getPagination());
    }
}

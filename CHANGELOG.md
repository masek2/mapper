# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0-rc5.1.0](https://gitlab.com/jaspr/mapper/compare/2.0.0-rc5.0...2.0.0-rc5.1.0) (2022-12-29)

## [2.0.0-rc5.0](https://gitlab.com/jaspr/mapper/compare/2.0.0-rc4.0...2.0.0-rc5.0) (2022-12-29)


### Fixed

* fix bug in CameFieldKebabUrl naming strategy ([f79bb33](https://gitlab.com/jaspr/mapper/commit/f79bb33a4e698fda0c6e56b2b2c75253e4e28d19))

## [2.0.0-rc4.0](https://gitlab.com/jaspr/mapper/compare/2.0.0-rc3.0...2.0.0-rc4.0) (2022-09-20)


### Added

* add NamingStrategy interface ([15823bb](https://gitlab.com/jaspr/mapper/commit/15823bb7c7143d590d74067bbbc8c9d0cf6e74e8))


### Changed

* remove unused methods and properties ([8776a5e](https://gitlab.com/jaspr/mapper/commit/8776a5e8bd48937bc0f361bcd2a864f3a0249f5a))

## [2.0.0-rc3.0](https://gitlab.com/jaspr/mapper/compare/2.0.0-rc2.0...2.0.0-rc3.0) (2022-09-19)


### Changed

* make possible use id field in filtering for resources ([5dc1f3e](https://gitlab.com/jaspr/mapper/commit/5dc1f3e555f6ef626c376b0dac1fe26e37e8e406))

## [2.0.0-rc2.0](https://gitlab.com/jaspr/mapper/compare/2.0.0-rc1.0...2.0.0-rc2.0) (2022-09-14)


### Fixed

* fix missing refactor changes ([d0ea2a5](https://gitlab.com/jaspr/mapper/commit/d0ea2a51880425895f2ca992b22b452e82afc516))

## [2.0.0-rc1.0](https://gitlab.com/jaspr/mapper/compare/1.0.0...2.0.0-rc1.0) (2022-09-12)


### ⚠ BREAKING CHANGES

*   * Remove LinkComposer
  * Rename URIParser -> Parser
  * Rename ClassMetadata methods getAttribute(s) to getAttribute(s)Data, getRelationship(s) to getRelationship(s)Data
  * ClassMetadata getAttributes now returns collection of attributes metadata, getRelationships too

### Fixed

* pass null if data not provided ([8f9006c](https://gitlab.com/jaspr/mapper/commit/8f9006c960b598d5b0145c3d0da58fe96ef0af5a))
* relationship name translation ([8718f4d](https://gitlab.com/jaspr/mapper/commit/8718f4d86d88680c222f8f7cae7ba146b4e09da5))
* return type of getRelationshipData can be null ([5e61fbb](https://gitlab.com/jaspr/mapper/commit/5e61fbb321247b9c3a3e4d9ec334feb756799944))
* translate id in related link ([3efd8a9](https://gitlab.com/jaspr/mapper/commit/3efd8a9613a8b2bf076d322ca6713417a9498071))


### Changed

* if withData is false, then data is not fetched at all ([1ad6e58](https://gitlab.com/jaspr/mapper/commit/1ad6e58b734a4d18036e6e70ee403b7e14e6def5))
* Implement changes from metadata ([71a0fd7](https://gitlab.com/jaspr/mapper/commit/71a0fd72d61c3a1d2fa448114db001d4c6a106cc))


### Added

* add ExpressionBuilderInterface ([00c2a46](https://gitlab.com/jaspr/mapper/commit/00c2a4665b5c9d58f0f7a854d9ca562bdef29b59))
* Add link for all to LinkFactory response ([d51c5dc](https://gitlab.com/jaspr/mapper/commit/d51c5dc1a9a1eb85b23f2be3a0b0fa0ee0cadce4))
* Add LinkFactory ([05b979f](https://gitlab.com/jaspr/mapper/commit/05b979f97ce05cae2244509f1a478cb50598a5f8))
* add related link to document links if relationship ([0592b3c](https://gitlab.com/jaspr/mapper/commit/0592b3ca0d66fc5f5008fca08890ec07a128ed57))
* add support for dto in attribute ([4eb9087](https://gitlab.com/jaspr/mapper/commit/4eb9087182b7d2ca7f7a50a94411c70d2f0bc849))
* Relationship::withData ([e4fb0c6](https://gitlab.com/jaspr/mapper/commit/e4fb0c67af121ec170b3afd0c5dcb37fedf5fa07))

## 1.0.0 (2022-06-20)


### Changed

* moving project ([94e53dd](https://gitlab.com/jaspr/mapper/commit/94e53ddf2dc0124c77a4482930cbd391fb0307b0))


### Fixed

* fix iterable attribute type recognition ([9639fdc](https://gitlab.com/jaspr/mapper/commit/9639fdc61913c880b06f67b8bd2b7f7e901519c1))
* several fixes after moving project ([50c2233](https://gitlab.com/jaspr/mapper/commit/50c22337d7710a8778a727aeffe4e9b0149dfe76))

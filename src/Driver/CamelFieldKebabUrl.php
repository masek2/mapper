<?php

/**
 * Created by uzivatel
 * at 20.09.2022 10:18
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\Inflector\InflectorInterface;

use function Symfony\Component\String\u;

/**
 * Class CamelFieldKebabUrl
 *
 * @package JSONAPI\Mapper\Driver
 */
class CamelFieldKebabUrl implements NamingStrategy
{
    /**
     * @var InflectorInterface
     */
    private InflectorInterface $inflector;

    public function __construct()
    {
        $this->inflector = new EnglishInflector();
    }

    /**
     * @inheritDoc
     */
    public function reflectionNameToField(string $name): string
    {
        return u($name)->camel()->toString();
    }

    /**
     * @inheritDoc
     */
    public function relationshipURLToField(string $relationshipName): string
    {
        return u($relationshipName)->camel()->toString();
    }

    /**
     * @inheritDoc
     */
    public function relationshipFieldToURL(string $relationshipName): string
    {
        return u($relationshipName)->snake()->replace('_', '-')->toString();
    }

    /**
     * Return kebab className pluralized
     * @inheritDoc
     */
    public function typeClassNameToField(string $className): string
    {
        $names = $this->inflector->pluralize(u($className)->snake()->replace('_', '-')->toString());
        return array_shift($names);
    }

    /**
     * @inheritDoc
     */
    public function typeFieldToURL(string $type): string
    {
        return u($type)->snake()->replace('_', '-')->toString();
    }

    /**
     * @inheritDoc
     */
    public function typeURLToField(string $type): string
    {
        return u($type)->snake()->replace('_', '-')->toString();
    }
}

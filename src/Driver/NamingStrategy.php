<?php

/**
 * Created by uzivatel
 * at 20.09.2022 9:58
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Driver;

interface NamingStrategy
{
    /**
     * @param string $name - PropertyReflection::getName()|MethodReflection::getName()
     *
     * @return string - Must return resource field name string used in Document
     */
    public function reflectionNameToField(string $name): string;

    /**
     * @param string $relationshipName - Relationship name in URIs path
     *
     * @return string - Must return field name used in resource metadata
     */
    public function relationshipURLToField(string $relationshipName): string;

    /**
     * @param string $relationshipName - Field name in resource metadata
     *
     * @return string - Must return relationship name used in URI
     */
    public function relationshipFieldToURL(string $relationshipName): string;

    /**
     * @param string $className - Resource short class name
     * @phpstan-param class-string $className
     *
     * @return string - Must return resource type field string used in Document
     */
    public function typeClassNameToField(string $className): string;

    /**
     * @param string $type - Resource type string
     *
     * @return string - Must return resource type string used in URI
     */
    public function typeFieldToURL(string $type): string;

    /**
     * @param string $type - URI type part string
     *
     * @return string - Must return resource type string used in class metadata
     */
    public function typeURLToField(string $type): string;
}

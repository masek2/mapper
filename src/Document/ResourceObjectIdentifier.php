<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Exception\Document\AlreadyInUse;
use stdClass;

/**
 * Class ResourceObjectIdentifier
 *
 * @package JSONAPI\Document
 */
class ResourceObjectIdentifier implements Serializable, HasMeta, PrimaryData
{
    use MetaExtension;

    /**
     * @var array<Field>
     */
    protected array $fields;

    /**
     * ResourceObjectIdentifier constructor.
     *
     * @param Type $type
     * @param Id   $id
     */
    public function __construct(Type $type, Id $id)
    {
        $this->fields = [];
        try {
            $this->addField($type);
            $this->addField($id);
        } catch (AlreadyInUse $ignored) {
            // NO-SONAR
        }
    }

    /**
     * @param Field $field
     *
     * @throws AlreadyInUse
     */
    protected function addField(Field $field): void
    {
        if (array_key_exists($field->getKey(), $this->fields)) {
            throw new AlreadyInUse($field->getKey());
        }
        $this->fields[$field->getKey()] = $field;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->fields[Members::ID]->getData();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->fields[Members::TYPE]->getData();
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return object data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): object
    {
        $ret       = new stdClass();
        $ret->type = $this->fields[Members::TYPE];
        $ret->id   = $this->fields[Members::ID];
        if ($this->hasMeta()) {
            $ret->meta = $this->getMeta();
        }
        return $ret;
    }
}

<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Encoding\ObjectCollection;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;

/**
 * Class Relationships
 *
 * @package JSONAPI\Document
 */
final class Relationship extends Field implements Serializable, HasLinks, HasMeta
{
    use LinksExtension;
    use MetaExtension;

    /**
     * @var ResourceObject owner
     */
    private ResourceObject $parent;
    /**
     * @var int|bool withData
     */
    private int|bool $withData;

    public function __construct(
        string $key,
        ResourceObject $parent,
        int|bool $withData = true
    ) {
        parent::__construct($key);
        $this->parent   = $parent;
        $this->withData = $withData;
    }

    /**
     * @return ResourceObject
     */
    public function getParent(): ResourceObject
    {
        return $this->parent;
    }

    /**
     * @param mixed $data
     *
     * @throws ForbiddenDataType
     */
    public function setData(mixed $data): void
    {
        if (
            $data instanceof ResourceObjectIdentifier ||
            $data instanceof ResourceCollection ||
            is_null($data)
        ) {
            parent::setData($data);
        } else {
            throw new ForbiddenDataType($this->getKey(), gettype($data));
        }
    }

    /**
     * @return object
     */
    public function jsonSerialize(): object
    {
        $ret = new \stdClass();
        if ($this->withData) {
            $ret->data = $this->getData();
        }
        if ($this->hasLinks()) {
            $ret->links = $this->getLinks();
        }
        if ($this->hasMeta()) {
            $ret->meta = $this->getMeta();
        }
        return $ret;
    }

    /**
     * @return ResourceObjectIdentifier|ResourceCollection|null
     */
    public function getData(): ResourceObjectIdentifier|ResourceCollection|null
    {
        return $this->data;
    }

    /**
     * @return bool|int
     */
    public function getWithData(): bool|int
    {
        return $this->withData;
    }
}

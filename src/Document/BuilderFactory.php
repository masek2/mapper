<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Configuration;
use JSONAPI\Mapper\Encoding\AttributesProcessor;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\LinksProcessor;
use JSONAPI\Mapper\Encoding\MetaProcessor;
use JSONAPI\Mapper\Encoding\RelationshipsProcessor;
use JSONAPI\Mapper\Exception\Http\BadRequest;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Parser;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class DocumentBuilderFactory
 *
 * @package JSONAPI\Factory
 */
final class BuilderFactory
{
    private Configuration $configuration;

    /**
     * DocumentBuilderFactory constructor.
     *
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return Builder
     * @throws BadRequest|MetadataException
     */
    public function create(ServerRequestInterface $request): Builder
    {

        $uri         = (new Parser($this->configuration))->parse($request);
        $linkFactory = new LinkFactory(
            $this->configuration->getMetadataRepository(),
            $this->configuration->getNamingStrategy()
        );
        $encoder     = new Encoder(
            $this->configuration->getMetadataRepository(),
            $this->configuration->getLogger()
        );
        $encoder->addProcessor(
            new AttributesProcessor(
                $this->configuration->getMetadataRepository(),
                $this->configuration->getLogger(),
                $uri->getFieldset()
            )
        );
        $encoder->addProcessor(
            new MetaProcessor(
                $this->configuration->getMetadataRepository(),
                $this->configuration->getLogger()
            )
        );
        $encoder->addProcessor(
            new RelationshipsProcessor(
                $encoder,
                $this->configuration->getMetadataRepository(),
                $this->configuration->getLogger(),
                $uri->getInclusion(),
                $uri->getFieldset()
            )
        );
        $encoder->addProcessor(
            new LinksProcessor(
                $linkFactory,
                $this->configuration->getBaseURL(),
                $uri->getPagination()
            )
        );
        $collector = new InclusionCollector(
            $this->configuration->getMetadataRepository(),
            $encoder,
            $this->configuration->getLogger()
        );
        return new Builder($encoder, $collector, $linkFactory, $uri, $this->configuration->getLogger());
    }
}

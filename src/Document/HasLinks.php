<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

/**
 * Interface HasLinks
 *
 * @package JSONAPI\Document
 */
interface HasLinks
{
    /**
     * @return Links
     */
    public function getLinks(): Links;

    /**
     * @return bool
     */
    public function hasLinks(): bool;
}

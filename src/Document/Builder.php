<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Exception\Document\DocumentException;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JSONAPI\Mapper\Exception\Driver\DriverException;
use JSONAPI\Mapper\Exception\Http\BadRequest;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\Helper\DoctrineProxyTrait;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Pagination\PaginationWithTotal;
use JSONAPI\Mapper\URI\ParsedURI;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Builder
 *
 * @package JSONAPI
 */
final class Builder
{
    use DoctrineProxyTrait;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Encoder
     */
    private Encoder $encoder;
    /**
     * @var Document
     */
    private Document $document;
    /**
     * @var LinkFactory
     */
    private LinkFactory $linkFactory;
    /**
     * @var InclusionCollector
     */
    private InclusionCollector $inclusionFetcher;

    private ParsedURI $uri;

    /**
     * DocumentBuilder constructor.
     *
     * @param Encoder              $encoder
     * @param InclusionCollector   $inclusionFetcher
     * @param LinkFactory          $linkFactory
     * @param ParsedURI            $uri
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        Encoder $encoder,
        InclusionCollector $inclusionFetcher,
        LinkFactory $linkFactory,
        ParsedURI $uri,
        LoggerInterface $logger = null
    ) {
        $this->encoder          = $encoder;
        $this->inclusionFetcher = $inclusionFetcher;
        $this->linkFactory      = $linkFactory;
        $this->uri              = $uri;
        $this->logger           = $logger ?? new NullLogger();
    }

    /**
     * @param iterable<object>|object|null $data
     *
     * @return $this
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     */
    public function setData(object|iterable|null $data): Builder
    {
        $this->logger->debug('Setting data.');
        $this->document = new Document();
        if (is_iterable($data)) {
            if (!$this->uri->getPath()->isCollection()) {
                throw new DocumentException("Collection provided for non-collection URI.");
            }
            $this->logger->debug('It is resource collection.');
            $collection = new ResourceCollection();
            foreach ($data as $item) {
                if ($this->uri->getPath()->isRelationship()) {
                    $collection->add($this->encoder->identify($item));
                } else {
                    $collection->add($this->encoder->encode($item));
                }
                if ($this->uri->getInclusion()->hasInclusions()) {
                    $this->inclusionFetcher->fetchInclusions($item, $this->uri->getInclusion()->getInclusions());
                }
            }
            $this->document->setData($collection);
        } elseif (is_object($data)) {
            $this->logger->debug('It is single resource');
            if ($this->uri->getPath()->isRelationship()) {
                $this->document->setData($this->encoder->identify($data));
            } else {
                $this->document->setData($this->encoder->encode($data));
            }
            if ($this->uri->getInclusion()->hasInclusions()) {
                $this->inclusionFetcher->fetchInclusions($data, $this->uri->getInclusion()->getInclusions());
            }
        } else {
            $this->document->setData(null);
        }
        $this->logger->debug('Data set.');
        return $this;
    }

    /**
     * Use this method if you want auto adding of links like prev, next, last etc...
     * If you do not provide this information, builder will generate prev, next and first link without checking of
     * total threshold
     *
     * @param int $total
     *
     * @return $this
     */
    public function setTotal(int $total): Builder
    {
        $this->logger->debug('Setting total.');
        if ($this->uri->getPagination() instanceof PaginationWithTotal) {
            $this->uri->getPagination()->setTotal($total);
            $this->logger->debug('Total set.');
        }
        return $this;
    }

    /**
     * @return Document
     * @throws MetadataException
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     */
    public function build(): Document
    {
        $this->logger->debug('Building doc.');
        $links = $this->linkFactory->getDocumentLinks($this->uri);
        foreach ($links as $name => $link) {
            $this->document->getLinks()->create($name, $this->uri->getBaseURL() . $link);
        }
        $this->logger->debug('Links set.');
        if ($this->uri->getInclusion()->hasInclusions()) {
            $this->document->setIncludes($this->inclusionFetcher->getIncluded());
        }
        $this->logger->debug('Includes set.');
        $this->logger->debug('Build complete.');
        return $this->document;
    }
}

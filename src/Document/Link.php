<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JetBrains\PhpStorm\Internal\TentativeType;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JsonSerializable;
use stdClass;

/**
 * Class Link
 *
 * @package JSONAPI\Document
 */
class Link implements HasMeta, JsonSerializable
{
    use MetaExtension;

    public const SELF = 'self';
    public const RELATED = 'related';
    public const FIRST = 'first';
    public const LAST = 'last';
    public const NEXT = 'next';
    public const PREV = 'prev';
    public const ABOUT = 'about';

    /**
     * @var string key
     */
    private string $key;
    /**
     * @var string url
     */
    private string $url;

    /**
     * Link constructor.
     *
     * @param string    $key
     * @param string    $url
     * @param Meta|null $meta
     *
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     */
    public function __construct(string $key, string $url, Meta $meta = null)
    {

        $this->key = $key;
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new ForbiddenDataType($this->getKey(), "Data are not valid URL.");
        }
        if (!preg_match(Members::NAME_REGEX, $key)) {
            throw new ForbiddenCharacter($key);
        }
        $this->key = $key;
        $this->url = $url;
        if (!is_null($meta)) {
            $this->setMeta($meta);
        }
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function jsonSerialize(): string|object
    {
        if ($this->hasMeta()) {
            $obj       = new stdClass();
            $obj->href = $this->url;
            $obj->meta = $this->meta;
            return $obj;
        } else {
            return $this->url;
        }
    }
}

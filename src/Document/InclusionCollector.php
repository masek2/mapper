<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Exception\Document\DocumentException;
use JSONAPI\Mapper\Exception\Document\InclusionOverflow;
use JSONAPI\Mapper\Exception\Driver\DriverException;
use JSONAPI\Mapper\Exception\Driver\ProcessorException;
use JSONAPI\Mapper\Exception\Http\BadRequest;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\Exception\Metadata\RelationNotFound;
use JSONAPI\Mapper\Helper\DoctrineProxyTrait;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\URI\Inclusion\Inclusion;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class InclusionCollector
 *
 * @package JSONAPI\Factory
 */
class InclusionCollector
{
    use DoctrineProxyTrait;

    /**
     * @var ResourceCollection
     */
    private ResourceCollection $included;
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $metadataRepository;
    /**
     * @var LoggerInterface|null
     */
    private ?LoggerInterface $logger;
    /**
     * @var Encoder
     */
    private Encoder $encoder;
    /**
     * InclusionFetcher constructor.
     *
     * @param MetadataRepository   $metadataRepository
     * @param Encoder              $encoder
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        MetadataRepository $metadataRepository,
        Encoder $encoder,
        LoggerInterface $logger = null
    ) {
        $this->included = new ResourceCollection();
        $this->metadataRepository = $metadataRepository;
        $this->encoder = $encoder;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * @param object      $object
     * @param Inclusion[] $inclusions
     *
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     */
    public function fetchInclusions(object $object, array $inclusions): void
    {
        $this->logger->debug('Fetching inclusions...');
        $classMetadata = $this->metadataRepository->getByClass(self::clearDoctrineProxyPrefix(get_class($object)));
        foreach ($inclusions as $sub) {
            try {
                $relationship = $classMetadata->getRelationship($sub->getRelationName());
                $data = null;
                if ($relationship->property) {
                    $data = $object->{$relationship->property};
                } elseif ($relationship->getter) {
                    $data = call_user_func([$object, $relationship->getter]);
                }
                if (!empty($data)) {
                    if ($relationship->isCollection) {
                        foreach ($data as $item) {
                            $this->addInclusion($item);
                            if ($sub->hasInclusions()) {
                                $this->fetchInclusions($item, $sub->getInclusions());
                            }
                        }
                    } else {
                        $this->addInclusion($data);
                        if ($sub->hasInclusions()) {
                            $this->fetchInclusions($data, $sub->getInclusions());
                        }
                    }
                }
            } catch (RelationNotFound $relationNotFound) {
                throw new BadRequest("URL malformed around '{$sub->getRelationName()}'.");
            }
        }
    }

    /**
     * @param object $item
     *
     * @return void
     * @throws ProcessorException
     * @throws \JSONAPI\Mapper\Exception\Document\ForbiddenCharacter
     * @throws \JSONAPI\Mapper\Exception\Document\ForbiddenDataType
     * @throws \JSONAPI\Mapper\Exception\Metadata\MetadataNotFound
     */
    private function addInclusion(object $item): void
    {
        $this->included->add($this->encoder->encode($item));
    }

    /**
     * @return ResourceCollection
     */
    public function getIncluded(): ResourceCollection
    {
        return $this->included;
    }
}

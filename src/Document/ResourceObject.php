<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Encoding\ObjectCollection;
use JSONAPI\Mapper\Exception\Document\AlreadyInUse;
use JSONAPI\Mapper\Exception\Document\AttributeNotExist;
use JSONAPI\Mapper\Exception\Document\RelationshipNotExist;

/**
 * Class ResourceObject
 *
 * @package JSONAPI\Document
 */
final class ResourceObject extends ResourceObjectIdentifier implements HasLinks, PrimaryData
{
    use LinksExtension;

    /**
     * @param Attribute $attribute
     *
     * @throws AlreadyInUse
     */
    public function addAttribute(Attribute $attribute): void
    {
        $this->addField($attribute);
    }

    /**
     * @param Relationship $relationship
     *
     * @throws AlreadyInUse
     */
    public function addRelationship(Relationship $relationship): void
    {
        $this->addField($relationship);
    }

    /**
     * Returns Attribute value
     *
     * @param string $key
     *
     * @return mixed
     * @throws AttributeNotExist
     */
    public function getAttributeData(string $key): mixed
    {
        if (!$this->hasAttribute($key)) {
            throw new AttributeNotExist($key);
        }
        return $this->fields[$key]->getData();
    }

    /**
     * @return array<string, mixed>
     */
    public function getAttributesData(): array
    {
        $ret = [];
        /** @var Attribute $attribute */
        foreach ($this->getAttributes() as $attribute) {
            $ret[$attribute->getKey()] = $attribute->getData();
        }
        return $ret;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasAttribute(string $key): bool
    {
        return array_key_exists($key, $this->getAttributes());
    }

    /**
     * @return array<Attribute>
     */
    public function getAttributes(): array
    {
        return array_filter($this->fields, fn($element) => $element instanceof Attribute, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Reruns Relationship value
     *
     * @param string $key
     *
     * @return ResourceObjectIdentifier|ResourceObjectIdentifier[]|null
     * @throws RelationshipNotExist
     */
    public function getRelationshipData(string $key): ResourceObjectIdentifier|array|null
    {
        if (!$this->hasRelationship($key)) {
            throw new RelationshipNotExist($key);
        }
        return $this->fields[$key]->getData();
    }

    /**
     * @return array<string, ResourceObjectIdentifier|ResourceObjectIdentifier[]|null>
     */
    public function getRelationshipsData(): array
    {
        $ret = [];
        /** @var Relationship $relationship */
        foreach ($this->getRelationships() as $relationship) {
            $ret[$relationship->getKey()] = $relationship->getData();
        }
        return $ret;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasRelationship(string $key): bool
    {
        return array_key_exists($key, $this->getRelationships());
    }

    /**
     * @return array<Relationship>
     */
    public function getRelationships(): array
    {
        return array_filter($this->fields, fn($element) => $element instanceof Relationship, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return object data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): object
    {
        $ret = parent::jsonSerialize();
        if (count($this->getAttributes()) > 0) {
            $ret->attributes = (object)$this->getAttributes();
        }
        if (count($this->getRelationships()) > 0) {
            $ret->relationships = (object)$this->getRelationships();
        }
        if ($this->hasLinks()) {
            $ret->links = $this->getLinks();
        }
        return $ret;
    }
}

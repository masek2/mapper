<?php

/**
 * Created by uzivatel
 * at 11.07.2022 14:07
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

interface Members
{
    public const NAME_REGEX = "/^([a-zA-Z0-9]+)([a-zA-Z-0-9_]*[a-zA-Z-0-9])?$/";
    public const DATA = 'data';
    public const ERRORS = 'errors';
    public const JSONAPI = 'jsonapi';
    public const INCLUDED = 'included';
    public const ATTRIBUTES = 'attributes';
    public const RELATIONSHIPS = 'relationships';
    public const META = 'meta';
    public const LINKS = 'links';
    public const ID = 'id';
    public const TYPE = 'type';
}

<?php

/**
 * Created by tomas
 * 08.07.2022 23:13
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Document;

use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JsonSerializable;

/**
 * Class Links
 *
 * @package JSONAPI\Mapper\Document
 */
class Links implements JsonSerializable, Members
{
    /**
     * @var Link[]
     */
    private array $links = [];

    /**
     * @param string    $name
     * @param string    $url
     * @param Meta|null $meta
     *
     * @return Link
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     */
    public function create(string $name, string $url, Meta $meta = null): Link
    {
        $link = new Link($name, $url, $meta);
        $this->add($link);
        return $link;
    }

    /**
     * @param Link $link
     *
     * @return void
     */
    public function add(Link $link): void
    {
        $this->links[$link->getKey()] = $link;
    }

    /**
     * @param string $key
     *
     * @return Link|null
     */
    public function find(string $key): ?Link
    {
        return $this->links[$key] ?? null;
    }

    /**
     * @param string $key
     *
     * @return void
     */
    public function remove(string $key): void
    {
        unset($this->links[$key]);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->links) === 0;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): object
    {
        return (object)$this->links;
    }
}

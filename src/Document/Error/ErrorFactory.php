<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Document\Error;

use JSONAPI\Mapper\Document\Error;
use Throwable;

/**
 * Interface Factory
 *
 * @package JSONAPI\Document\Error
 */
interface ErrorFactory
{
    /**
     * Method transform Throwable to Error
     *
     * @param Throwable $exception
     *
     * @return Error
     */
    public function fromThrowable(Throwable $exception): Error;
}

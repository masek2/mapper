<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:53
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception\Driver;

/**
 * Class ProcessorException
 *
 * @package JSONAPI\Mapper\Exception\Driver
 */
class ProcessorException extends DriverException
{
}

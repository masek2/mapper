<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception\Http;

use JSONAPI\Mapper\Exception\HasParameter;

/**
 * Class ExpressionException
 *
 * @package JSONAPI\URI\Filtering
 */
class ExpressionException extends BadRequest implements HasParameter
{
    public function getParameter(): string
    {
        return 'filter';
    }
}

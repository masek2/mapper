<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception\Document;

use JSONAPI\Mapper\Exception\HasPointer;

/**
 * Class AttributeNotExist
 *
 * @package JSONAPI\Exception\Document
 */
class AttributeNotExist extends FieldNotExist implements HasPointer
{
    /**
     * @return string
     */
    public function getPointer(): string
    {
        return '/data/attributes/' . $this->field;
    }
}

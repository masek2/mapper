<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Exception\Document;

use JSONAPI\Mapper\Exception\HasPointer;

/**
 * Class RelationshipNotExist
 *
 * @package JSONAPI\Exception\Document
 */
class RelationshipNotExist extends FieldNotExist implements HasPointer
{
    /**
     * @return string
     */
    public function getPointer(): string
    {
        return '/data/relationships/' . $this->field;
    }
}

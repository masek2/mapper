<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Encoding\ObjectCollection;
use JSONAPI\Mapper\Exception\Metadata\AlreadyInUse;
use JSONAPI\Mapper\Exception\Metadata\AttributeNotFound;
use JSONAPI\Mapper\Exception\Metadata\RelationNotFound;
use JSONAPI\Mapper\Metadata;

/**
 * Class ClassMetadata
 *
 * @package JSONAPI
 */
final class ClassMetadata
{
    /**
     * @var string
     */
    private string $className;
    /**
     * @var string
     */
    private string $type;

    /**
     * @var Metadata\Id
     */
    private Metadata\Id $id;

    /**
     * @var array<Field>
     */
    private array $fields;
    /**
     * @var Meta|null
     */
    private ?Meta $meta;
    /**
     * @var array<string> methods
     */
    private array $methods;

    /**
     * ClassMetadata constructor.
     *
     * @param string         $className
     * @param string         $type
     * @param Metadata\Id    $id
     * @param Attribute[]    $attributes
     * @param Relationship[] $relationships
     * @param Meta|null      $resourceMeta
     * @param array<string>  $methods
     *
     * @throws AlreadyInUse
     */
    public function __construct(
        string $className,
        string $type,
        Metadata\Id $id,
        array $attributes,
        array $relationships,
        ?Meta $resourceMeta = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ]
    ) {
        $this->fields                = [];
        $this->className             = $className;
        $this->id                    = $id;
        $this->type                  = $type;
        $this->meta                  = $resourceMeta;
        $this->fields[Members::ID]   = $id;
        $this->fields[Members::TYPE] = $type;
        foreach ($attributes as $attribute) {
            if (array_key_exists($attribute->name, $this->fields)) {
                throw new AlreadyInUse($attribute->name);
            }
            $this->fields[$attribute->name] = $attribute;
        }
        foreach ($relationships as $relationship) {
            if (array_key_exists($relationship->name, $this->fields)) {
                throw new AlreadyInUse($relationship->name);
            }
            $this->fields[$relationship->name] = $relationship;
        }
        $this->methods = $methods;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Meta|null
     */
    public function getMeta(): ?Meta
    {
        return $this->meta;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return array_filter($this->fields, fn($i) => $i instanceof Attribute, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * @param string $name
     *
     * @return Attribute
     * @throws AttributeNotFound
     */
    public function getAttribute(string $name): Attribute
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttributes()[$name];
        }
        throw new AttributeNotFound($name, $this->type);
    }

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function hasAttribute(string $fieldName): bool
    {
        return array_key_exists($fieldName, $this->getAttributes());
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return array_filter($this->fields, fn($i) => $i instanceof Relationship, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * @param string $name
     *
     * @return Relationship
     * @throws RelationNotFound
     */
    public function getRelationship(string $name): Relationship
    {
        if ($this->hasRelationship($name)) {
            return $this->getRelationships()[$name];
        }
        throw new RelationNotFound($name, $this->getType());
    }

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function hasRelationship(string $fieldName): bool
    {
        return array_key_exists($fieldName, $this->getRelationships());
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function hasField(string $fieldName): bool
    {
        return array_key_exists($fieldName, $this->fields);
    }

    /**
     * @return array<string>
     */
    public function getSupportedMethods(): array
    {
        return $this->methods;
    }

    /**
     * @param string $method
     *
     * @return bool
     */
    public function isMethodSupported(string $method): bool
    {
        return in_array($method, $this->methods);
    }
}

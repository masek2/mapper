<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;

/**
 * Class MetadataRepository
 *
 * @package JSONAPI\Metadata
 */
class MetadataRepository
{
    /**
     * @var array<ClassMetadata>
     */
    private array $collection;
    /**
     * @var array<string>
     */
    private array $typeToClassMap;

    public function __construct()
    {
        $this->collection     = [];
        $this->typeToClassMap = [];
    }

    /**
     * @param string $type - resource type
     *
     * @return ClassMetadata
     * @throws MetadataNotFound
     */
    public function getByType(string $type): ClassMetadata
    {
        if (array_key_exists($type, $this->typeToClassMap)) {
            return $this->getByClass($this->typeToClassMap[$type]);
        }
        throw new MetadataNotFound($type);
    }

    /**
     * @param string $className
     *
     * @return ClassMetadata
     * @throws MetadataNotFound
     */
    public function getByClass(string $className): ClassMetadata
    {
        if (array_key_exists($className, $this->collection)) {
            return $this->collection[$className];
        }
        throw new MetadataNotFound($className);
    }

    /**
     * @return ClassMetadata[]
     */
    public function getAll(): array
    {
        return $this->collection;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function add(ClassMetadata $metadata): void
    {
        $this->collection[$metadata->getClassName()] = $metadata;
        $this->typeToClassMap[$metadata->getType()]  = $metadata->getClassName();
    }
}

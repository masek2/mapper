<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

/**
 * Class Common
 *
 * @package JSONAPI\Metadata
 */
abstract class Field
{
    /**
     * @var string|null setter
     */
    public ?string $setter = null;
    /**
     * @var string|null getter
     */
    public ?string $getter = null;
    /**
     * @var string|null name
     */
    public ?string $name = null;
    /**
     * @var string|null property
     */
    public ?string $property = null;
    /**
     * @var bool|null nullable
     */
    public ?bool $nullable = null;

    /**
     * Field constructor.
     *
     * @param string|null $name
     * @param string|null $property
     * @param string|null $getter
     * @param string|null $setter
     * @param bool|null   $nullable
     */
    public function __construct(
        ?string $name = null,
        ?string $property = null,
        ?string $getter = null,
        ?string $setter = null,
        ?bool $nullable = null
    ) {
        $this->name     = $name;
        $this->property = $property;
        $this->getter   = $getter;
        $this->setter   = $setter;
        $this->nullable = $nullable;
    }
}

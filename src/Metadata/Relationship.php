<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Metadata;

use Fig\Http\Message\RequestMethodInterface;

/**
 * Class Relationship
 *
 * @package JSONAPI\Metadata
 */
class Relationship extends Field
{
    /**
     * @var bool|null isCollection
     */
    public ?bool $isCollection = null;
    /**
     * @var Meta|null meta
     */
    public ?Meta $meta = null;
    /**
     * @var string|null target
     */
    public ?string $target;
    /**
     * @var array<string> methods
     */
    public array $methods;

    /**
     * @var int|bool withData -
     *      true - fetch data, do not limit them,
     *      false - do not fetch data,
     *      int - fetch data, but limit them
     */
    public int|bool $withData = 25;

    /**
     * Relationship constructor.
     *
     * @param string|null   $target
     * @param string|null   $name
     * @param string|null   $property
     * @param string|null   $getter
     * @param string|null   $setter
     * @param bool|null     $isCollection
     * @param Meta|null     $meta
     * @param bool|null     $nullable
     * @param array<string> $methods
     * @param int|bool      $withData
     */
    protected function __construct(
        ?string $target,
        string $name = null,
        string $property = null,
        string $getter = null,
        string $setter = null,
        ?bool $isCollection = null,
        ?Meta $meta = null,
        ?bool $nullable = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ],
        int|bool $withData = 25
    ) {
        parent::__construct($name, $property, $getter, $setter, $nullable);
        $this->target       = $target;
        $this->meta         = $meta;
        $this->isCollection = $isCollection;
        $this->methods      = $methods;
        $this->withData     = $withData;
    }


    /**
     * @param string        $property
     * @param string        $target
     * @param string|null   $name
     * @param bool          $isCollection
     * @param Meta|null     $meta
     * @param bool|null     $nullable
     * @param array<string> $methods
     * @param int|bool      $withData
     *
     * @return Relationship
     */
    public static function createByProperty(
        string $property,
        string $target,
        string $name = null,
        bool $isCollection = null,
        Meta $meta = null,
        bool $nullable = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ],
        int|bool $withData = 25,
    ): Relationship {
        return new self($target, $name, $property, null, null, $isCollection, $meta, $nullable, $methods, $withData);
    }

    /**
     * @param string        $getter
     * @param string        $target
     * @param string|null   $setter
     * @param string|null   $name
     * @param bool          $isCollection
     * @param Meta|null     $meta
     * @param bool|null     $nullable
     * @param array<string> $methods
     * @param int|bool      $withData
     *
     * @return Relationship
     */
    public static function createByMethod(
        string $getter,
        string $target,
        string $setter = null,
        string $name = null,
        bool $isCollection = null,
        Meta $meta = null,
        bool $nullable = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ],
        int|bool $withData = 25
    ): Relationship {
        return new self($target, $name, null, $getter, $setter, $isCollection, $meta, $nullable, $methods, $withData);
    }
}

<?php

/**
 * Created by uzivatel
 * at 20.06.2022 13:06
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI;

use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Driver\CamelFieldKebabUrl;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\URI\Filtering\FilterInterface;
use JSONAPI\Mapper\URI\Inclusion\InclusionInterface;
use JSONAPI\Mapper\URI\Pagination\PaginationInterface;
use JSONAPI\Mapper\URI\Path\PathInterface;
use JSONAPI\Mapper\URI\Sorting\SortInterface;

use function Symfony\Component\String\s;

/**
 * Class LinkFactory
 *
 * @package JSONAPI\Mapper\URI
 */
class LinkFactory
{
    /**
     * @var MetadataRepository repository
     */
    private MetadataRepository $repository;
    /**
     * @var NamingStrategy namingStrategy
     */
    private NamingStrategy $namingStrategy;

    /**
     * @param MetadataRepository  $repository
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(MetadataRepository $repository, NamingStrategy $namingStrategy = null)
    {
        $this->repository     = $repository;
        $this->namingStrategy = $namingStrategy ?? new CamelFieldKebabUrl();
    }

    /**
     * @param string $type
     *
     * @return array<string, string|array<string,string>>
     * @throws MetadataNotFound
     */
    public function getResourceLinksByType(string $type): array
    {
        $metadata = $this->repository->getByType($type);
        return $this->getResourceLinks($metadata);
    }

    /**
     * @param string $className
     *
     * @return array<string, string|array<string,string>>
     * @throws MetadataNotFound
     */
    public function getResourceLinksByClass(string $className): array
    {
        $metadata = $this->repository->getByClass($className);
        return $this->getResourceLinks($metadata);
    }

    /**
     * @param ClassMetadata $metadata
     *
     * @return array<string, string|array<string,string>>
     * @example [
     *          self => '/resources/{id}',
     *          relationship1 => [
     *              self => '/resources/{id}/relationships/relationship1'
     *              related => '/resources/{id}/relationship1'
     *          ]
     * ]
     */
    public function getResourceLinks(ClassMetadata $metadata): array
    {
        $links             = [];
        $collection        = '/' . rawurlencode($this->namingStrategy->typeFieldToURL($metadata->getType()));
        $links['all']      = $collection;
        $self              = $collection . '/{id}';
        $links[Link::SELF] = $self;
        foreach ($metadata->getRelationships() as $relationship) {
            $name                       = $this->namingStrategy->relationshipFieldToURL($relationship->name);
            $links[$relationship->name] = [
                Link::SELF    => $self . '/relationships/' . rawurlencode($name),
                Link::RELATED => $self . '/' . rawurlencode($name)
            ];
        }
        return $links;
    }

    /**
     * @param ParsedURI $uri
     *
     * @return array<string, string>
     * @throws MetadataException
     */
    public function getDocumentLinks(ParsedURI $uri): array
    {
        $links      = [];
        $path       = $uri->getPath();
        $filter     = $uri->getFilter();
        $inclusion  = $uri->getInclusion();
        $fieldset   = $uri->getFieldset();
        $sort       = $uri->getSort();
        $pagination = $uri->getPagination();
        if ($uri->getPath()->isCollection()) {
            $links[Link::SELF]  = $this->createDocumentLink(
                $path,
                $filter,
                $inclusion,
                $fieldset,
                $pagination,
                $sort
            );
            $first              = $pagination->first();
            $links[Link::FIRST] = $this->createDocumentLink(
                $path,
                $filter,
                $inclusion,
                $fieldset,
                $first,
                $sort
            );
            if ($last = $pagination->last()) {
                $links[Link::LAST] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $last,
                    $sort
                );
            }
            if ($prev = $pagination->prev()) {
                $links[Link::PREV] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $prev,
                    $sort
                );
            }
            if ($next = $pagination->next()) {
                $links[Link::NEXT] = $this->createDocumentLink(
                    $path,
                    $filter,
                    $inclusion,
                    $fieldset,
                    $next,
                    $sort
                );
            }
        } else {
            $links[Link::SELF] = $this->createDocumentLink(
                $path,
                null,
                $inclusion,
                $fieldset,
                null,
                null
            );
        }
        if ($path->isRelationship()) {
            $resourceLinks        = $this->getResourceLinksByType($path->getResourceType());
            $related              = $resourceLinks[$path->getRelationshipName()][Link::RELATED];
            $links[Link::RELATED] = str_replace('{id}', $path->getId(), $related);
        }
        return $links;
    }

    /**
     * @param PathInterface            $path
     * @param FilterInterface|null     $filter
     * @param InclusionInterface|null  $inclusion
     * @param FieldsetInterface|null   $fieldset
     * @param PaginationInterface|null $pagination
     * @param SortInterface|null       $sort
     *
     * @return string
     */
    private function createDocumentLink(
        PathInterface $path,
        ?FilterInterface $filter,
        ?InclusionInterface $inclusion,
        ?FieldsetInterface $fieldset,
        ?PaginationInterface $pagination,
        ?SortInterface $sort
    ): string {
        $link = '' . $path;
        $mark = '?';
        if (strlen((string)$filter)) {
            $link .= $mark . $filter;
            $mark = '&';
        }
        if (strlen((string)$inclusion)) {
            $link .= $mark . $inclusion;
            $mark = '&';
        }
        if (strlen((string)$fieldset)) {
            $link .= $mark . $fieldset;
            $mark = '&';
        }
        if (strlen((string)$pagination)) {
            $link .= $mark . $pagination;
            $mark = '&';
        }
        if (strlen((string)$sort)) {
            $link .= $mark . $sort;
        }
        return $link;
    }
}

<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:35
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI;

use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\URI\Filtering\FilterInterface;
use JSONAPI\Mapper\URI\Inclusion\InclusionInterface;
use JSONAPI\Mapper\URI\Pagination\PaginationInterface;
use JSONAPI\Mapper\URI\Path\PathInterface;
use JSONAPI\Mapper\URI\Sorting\SortInterface;

/**
 * Interface ParsedURI
 *
 * @package JSONAPI\URI
 */
interface ParsedURI
{
    /**
     * @return FilterInterface
     */
    public function getFilter(): FilterInterface;

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface;

    /**
     * @return SortInterface
     */
    public function getSort(): SortInterface;

    /**
     * @return FieldsetInterface
     */
    public function getFieldset(): FieldsetInterface;

    /**
     * @return InclusionInterface

     */
    public function getInclusion(): InclusionInterface;

    /**
     * @return PathInterface
     */
    public function getPath(): PathInterface;

    /**
     * @return string
     */
    public function getBaseURL(): string;
}

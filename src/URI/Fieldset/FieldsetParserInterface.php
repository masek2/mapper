<?php

/**
 * Created by lasicka@logio.cz
 * at 06.10.2021 13:11
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Fieldset;

use JSONAPI\Mapper\Exception\Http\BadRequest;

interface FieldsetParserInterface
{
    /**
     * @param array<string, string>|null $data
     *
     * @return FieldsetInterface
     * @throws BadRequest
     */
    public function parse(?array $data): FieldsetInterface;
}

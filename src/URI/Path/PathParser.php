<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Path;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Driver\CamelFieldKebabUrl;
use JSONAPI\Mapper\Driver\NamingStrategy;
use JSONAPI\Mapper\Exception\Http\BadRequest;
use JSONAPI\Mapper\Metadata\MetadataRepository;

use function Symfony\Component\String\u;

/**
 * Class PathParser
 *
 * @package JSONAPI\URI\Path
 */
class PathParser implements PathInterface, PathParserInterface
{
    /**
     * @var string
     */
    private string $resource = '';

    /**
     * @var string|null
     */
    private ?string $id = null;

    /**
     * @var string|null
     */
    private ?string $relationship = null;

    /**
     * @var bool
     */
    private bool $isRelationship = false;
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $metadataRepository;
    /**
     * @var string
     */
    private string $method = RequestMethodInterface::METHOD_GET;
    /**
     * @var string baseURL
     */
    private string $baseURL;

    private NamingStrategy $namingStrategy;

    /**
     * PathParser constructor.
     *
     * @param MetadataRepository  $metadataRepository
     * @param string              $baseURL
     * @param NamingStrategy|null $namingStrategy
     */
    public function __construct(
        MetadataRepository $metadataRepository,
        string $baseURL,
        NamingStrategy $namingStrategy = null
    ) {
        $this->metadataRepository = $metadataRepository;
        $this->baseURL            = $baseURL;
        $this->namingStrategy     = $namingStrategy ?? new CamelFieldKebabUrl();
    }

    /**
     * @inheritDoc
     */
    public function parse(string $data, string $method): PathInterface
    {
        $this->reset();
        $this->method = $method;
        $req          = explode('/', $data);
        $base         = explode('/', parse_url($this->baseURL, PHP_URL_PATH) ?? '');
        $diff         = array_diff($req, $base);
        $data         = '/' . ltrim(implode('/', $diff), '/');
        //phpcs:ignore
        $pattern = '~^\/(?P<resource>[a-zA-Z0-9-_]+)(\/(?P<id>[^/]+)?((\/relationships\/(?P<relationship>[a-zA-Z0-9-_]+))|(\/(?P<related>[a-zA-Z0-9-_]+)))?)?$~'; //NOSONAR
        if (preg_match($pattern, $data, $matches)) {
            foreach (['resource', 'id', 'relationship', 'related'] as $key) {
                if (isset($matches[$key]) && strlen($matches[$key]) > 0) {
                    if ($key === 'relationship') {
                        $this->isRelationship = true;
                    }
                    if ($key === 'related') {
                        $this->relationship = $matches[$key];
                    } else {
                        $this->{$key} = $matches[$key];
                    }
                }
            }
        } else {
            throw new BadRequest();
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isRelationship(): bool
    {
        return $this->isRelationship;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $str = '/' . rawurlencode($this->resource);
        if ($this->id) {
            $str .= '/' . rawurlencode($this->id);
            if ($this->relationship) {
                if ($this->isRelationship) {
                    $str .= '/relationships';
                }
                $str .= '/' . rawurlencode($this->relationship);
            }
        }
        return $str;
    }

    /**
     * @inheritDoc
     */
    public function getPrimaryResourceType(): string
    {
        if ($this->getRelationshipName()) {
            return $this->metadataRepository
                ->getByClass(
                    $this->metadataRepository
                        ->getByType($this->getResourceType())
                        ->getRelationship($this->getRelationshipName())
                        ->target
                )
                ->getType();
        } else {
            return $this->metadataRepository->getByType($this->getResourceType())->getType();
        }
    }

    /**
     * @inheritDoc
     */
    public function getRelationshipName(): ?string
    {
        return $this->relationship ? $this->namingStrategy->relationshipURLToField($this->relationship) : null;
    }

    /**
     * @inheritDoc
     */
    public function getResourceType(): string
    {
        return $this->namingStrategy->typeURLToField($this->resource);
    }

    /**
     * @inheritDoc
     */
    public function isCollection(): bool
    {
        if ($this->getRelationshipName()) {
            return $this->metadataRepository
                ->getByType($this->getResourceType())
                ->getRelationship($this->getRelationshipName())
                ->isCollection;
        }
        if ($this->getId() || (strtoupper($this->method) === RequestMethodInterface::METHOD_POST)) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return void
     */
    private function reset(): void
    {
        $this->relationship   = null;
        $this->isRelationship = false;
        $this->id             = null;
        $this->resource       = '';
    }
}

<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Sorting;

use JSONAPI\Mapper\URI\QueryPartInterface;

/**
 * Interface SortInterface
 *
 * @package JSONAPI\URI\Sorting
 */
interface SortInterface extends QueryPartInterface
{
    public const ASC  = 'ASC';
    public const DESC = 'DESC';

    /**
     * @return array<string, string> associative array contains field as key and order as value
     * @example [
     *      "fieldA" => "DESC",
     *      "fieldB" => "ASC"
     * ]
     */
    public function getOrder(): array;
}

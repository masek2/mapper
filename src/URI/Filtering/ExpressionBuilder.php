<?php

/**
 * Created by tomas
 * 10.09.2022 20:27
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering;

use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Exception\HeterogeneousArrayError;
use JSONAPI\Expression\Exception\IncomparableExpressions;
use JSONAPI\Expression\Exception\InvalidArgument;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;
use JSONAPI\Mapper\Exception\Http\ExpressionException;

/**
 * Class ExpressionBuilder
 * Default implementation of expression builder with JASPR Expression library
 *
 * @package JSONAPI\Mapper\URI\Filtering
 */
class ExpressionBuilder implements ExpressionBuilderInterface
{
    /**
     * @inheritDoc
     */
    public function eq(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::eq($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function ne(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::ne($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function lt(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::lt($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function le(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::le($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function gt(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::gt($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function ge(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::ge($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function in(mixed $left, mixed $right): TBoolean
    {
        try {
            return Ex::in($left, $right);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function has(mixed $right, mixed $left): TBoolean
    {
        try {
            return Ex::has($right, $left);
        } catch (IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function be(mixed $left, mixed $from, mixed $to): TBoolean
    {
        try {
            return Ex::be($left, $from, $to);
        } catch (HeterogeneousArrayError | IncomparableExpressions $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function and(mixed $left, mixed $right): TBoolean
    {
        return Ex::and($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function or(mixed $left, mixed $right): TBoolean
    {
        return Ex::or($left, $right);
    }

    /**
     * @inheritDoc
     */
    public function not(mixed $expression): TBoolean
    {
        return Ex::not($expression);
    }

    /**
     * @inheritDoc
     */
    public function length(mixed $subject): TNumeric
    {
        return Ex::length($subject);
    }

    /**
     * @inheritDoc
     */
    public function concat(mixed $subject, mixed $append): TString
    {
        return Ex::concat($subject, $append);
    }

    /**
     * @inheritDoc
     */
    public function contains(mixed $haystack, mixed $needle): TBoolean
    {
        return Ex::contains($haystack, $needle);
    }

    /**
     * @inheritDoc
     */
    public function startsWith(mixed $haystack, mixed $needle): TBoolean
    {
        return Ex::startsWith($haystack, $needle);
    }

    /**
     * @inheritDoc
     */
    public function endsWith(mixed $haystack, mixed $needle): TBoolean
    {
        return Ex::endsWith($haystack, $needle);
    }

    /**
     * @inheritDoc
     */
    public function indexOf(mixed $haystack, mixed $needle): TNumeric
    {
        return Ex::indexOf($haystack, $needle);
    }

    /**
     * @inheritDoc
     */
    public function substring(mixed $string, mixed $start, mixed $length = null): TString
    {
        return Ex::substring($string, $start, $length);
    }

    /**
     * @inheritDoc
     */
    public function matchesPattern(mixed $subject, mixed $pattern): TBoolean
    {
        return Ex::matchesPattern($subject, $pattern);
    }

    /**
     * @inheritDoc
     */
    public function toLower(mixed $subject): TString
    {
        return Ex::toLower($subject);
    }

    /**
     * @inheritDoc
     */
    public function toUpper(mixed $subject): TString
    {
        return Ex::toUpper($subject);
    }

    /**
     * @inheritDoc
     */
    public function trim(mixed $subject): TString
    {
        return Ex::trim($subject);
    }

    /**
     * @inheritDoc
     */
    public function add(mixed $x, mixed $y): TNumeric
    {
        return Ex::add($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function sub(mixed $x, mixed $y): TNumeric
    {
        return Ex::sub($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function mul(mixed $x, mixed $y): TNumeric
    {
        return Ex::mul($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function div(mixed $x, mixed $y): TNumeric
    {
        return Ex::div($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function mod(mixed $x, mixed $y): TNumeric
    {
        return Ex::mod($x, $y);
    }

    /**
     * @inheritDoc
     */
    public function ceiling(mixed $value): TNumeric
    {
        return Ex::ceiling($value);
    }

    /**
     * @inheritDoc
     */
    public function floor(mixed $value): TNumeric
    {
        return Ex::floor($value);
    }

    /**
     * @inheritDoc
     */
    public function round(mixed $value): TNumeric
    {
        return Ex::round($value);
    }

    /**
     * @inheritDoc
     */
    public function date(mixed $datetime): TDateTime
    {
        return Ex::date($datetime);
    }

    /**
     * @inheritDoc
     */
    public function day(mixed $datetime): TNumeric
    {
        return Ex::day($datetime);
    }

    /**
     * @inheritDoc
     */
    public function hour(mixed $datetime): TNumeric
    {
        return Ex::hour($datetime);
    }

    /**
     * @inheritDoc
     */
    public function minute(mixed $datetime): TNumeric
    {
        return Ex::minute($datetime);
    }

    /**
     * @inheritDoc
     */
    public function month(mixed $datetime): TNumeric
    {
        return Ex::month($datetime);
    }

    /**
     * @inheritDoc
     */
    public function second(mixed $datetime): TNumeric
    {
        return Ex::second($datetime);
    }

    /**
     * @inheritDoc
     */
    public function time(mixed $datetime): TDateTime
    {
        return Ex::time($datetime);
    }

    /**
     * @inheritDoc
     */
    public function year(mixed $datetime): TNumeric
    {
        return Ex::year($datetime);
    }

    /**
     * @inheritDoc
     */
    public function literal(mixed $value): TNumeric|TString|TArray|TDateTime|TBoolean
    {
        try {
            return Ex::literal($value);
        } catch (InvalidArgument $e) {
            throw new ExpressionException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function field(mixed $name, mixed $type): TNumeric|TString|TArray|TDateTime|TBoolean
    {
        try {
            return Ex::field($name, $type);
        } catch (InvalidArgument $e) {
            throw new ExpressionException($e->getMessage());
        }
    }
}

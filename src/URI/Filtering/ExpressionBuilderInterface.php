<?php

/**
 * Created by tomas
 * 10.09.2022 20:01
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering;

use JSONAPI\Mapper\Exception\Http\ExpressionException;

interface ExpressionBuilderInterface
{
    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function eq(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function ne(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function lt(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function le(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function gt(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function ge(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function in(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function has(mixed $right, mixed $left): mixed;

    /**
     * @param mixed $left
     * @param mixed $from
     * @param mixed $to
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function be(mixed $left, mixed $from, mixed $to): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function and(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $left
     * @param mixed $right
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function or(mixed $left, mixed $right): mixed;

    /**
     * @param mixed $expression
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function not(mixed $expression): mixed;

    /**
     * @param mixed $subject
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function length(mixed $subject): mixed;

    /**
     * @param mixed $subject
     * @param mixed $append
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function concat(mixed $subject, mixed $append): mixed;

    /**
     * @param mixed $haystack
     * @param mixed $needle
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function contains(mixed $haystack, mixed $needle): mixed;

    /**
     * @param mixed $haystack
     * @param mixed $needle
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function startsWith(mixed $haystack, mixed $needle): mixed;

    /**
     * @param mixed $haystack
     * @param mixed $needle
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function endsWith(mixed $haystack, mixed $needle): mixed;

    /**
     * @param mixed $haystack
     * @param mixed $needle
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function indexOf(mixed $haystack, mixed $needle): mixed;

    /**
     * @param mixed      $string
     * @param mixed      $start
     * @param mixed|null $length
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function substring(mixed $string, mixed $start, mixed $length = null): mixed;

    /**
     * @param mixed $subject
     * @param mixed $pattern
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function matchesPattern(mixed $subject, mixed $pattern): mixed;

    /**
     * @param mixed $subject
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function toLower(mixed $subject): mixed;

    /**
     * @param mixed $subject
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function toUpper(mixed $subject): mixed;

    /**
     * @param mixed $subject
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function trim(mixed $subject): mixed;

    /**
     * @param mixed $x
     * @param mixed $y
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function add(mixed $x, mixed $y): mixed;

    /**
     * @param mixed $x
     * @param mixed $y
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function sub(mixed $x, mixed $y): mixed;

    /**
     * @param mixed $x
     * @param mixed $y
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function mul(mixed $x, mixed $y): mixed;

    /**
     * @param mixed $x
     * @param mixed $y
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function div(mixed $x, mixed $y): mixed;

    /**
     * @param mixed $x
     * @param mixed $y
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function mod(mixed $x, mixed $y): mixed;

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function ceiling(mixed $value): mixed;

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function floor(mixed $value): mixed;

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function round(mixed $value): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function date(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function day(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function hour(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function minute(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function month(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function second(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function time(mixed $datetime): mixed;

    /**
     * @param mixed $datetime
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function year(mixed $datetime): mixed;

    /**
     * @param mixed $value
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function literal(mixed $value): mixed;

    /**
     * @param mixed $name
     * @param mixed $type
     *
     * @return mixed
     * @throws ExpressionException
     */
    public function field(mixed $name, mixed $type): mixed;
}

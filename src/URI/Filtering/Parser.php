<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering;

use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Type\TArray;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;
use JSONAPI\Mapper\Document\Convertible;
use JSONAPI\Mapper\Exception\Http\ExpressionException;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\Metadata\ClassMetadata;
use JSONAPI\Mapper\Metadata\MetadataRepository;

abstract class Parser implements FilterParserInterface
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;
    /**
     * @var ExpressionBuilderInterface
     */
    private ExpressionBuilderInterface $builder;
    /**
     * @var ClassMetadata
     */
    private ClassMetadata $metadata;

    /**
     * @param MetadataRepository              $repository
     * @param ExpressionBuilderInterface|null $builder
     */
    public function __construct(
        MetadataRepository $repository,
        ExpressionBuilderInterface $builder = null
    ) {
        $this->repository = $repository;
        $this->builder    = $builder ?? new ExpressionBuilder();
    }

    /**
     * @inheritDoc
     */
    public function parse(mixed $data, string $className): FilterInterface
    {
        $this->metadata = $this->repository->getByClass($className);
        return new class ($data) implements FilterInterface {
            public function __construct(private readonly mixed $data)
            {
            }

            public function getCondition(): mixed
            {
                return $this->data;
            }

            public function __toString(): string
            {
                return (string)$this->data;
            }
        };
    }

    /**
     * @return ClassMetadata
     */
    protected function getMetadata(): ClassMetadata
    {
        return $this->metadata;
    }

    /**
     * @return MetadataRepository
     */
    protected function getRepository(): MetadataRepository
    {
        return $this->repository;
    }

    protected function ex(): ExpressionBuilderInterface
    {
        return $this->builder;
    }

    /**
     * @param string $identifier
     *
     * @return mixed
     * @throws ExpressionException
     */
    protected function parseField(string $identifier): mixed
    {
        $type = 'string';
        try {
            $classMetadata = $this->getMetadata();
            $parts         = [...explode(".", $identifier)];
            while ($part = array_shift($parts)) {
                if ($classMetadata->hasRelationship($part)) {
                    $classMetadata = $this->getRepository()->getByClass(
                        $classMetadata->getRelationship($part)->target
                    );
                } elseif ($classMetadata->hasAttribute($part)) {
                    $type = $classMetadata->getAttribute($part)->type;
                    if (is_a($type, Convertible::class, true)) {
                        // skip because it's DTO object
                        return $this->ex()->field($identifier, 'mixed');
                    }
                    if (!is_null($classMetadata->getAttribute($part)->of)) {
                        $type = $classMetadata->getAttribute($part)->of . '[]';
                    }
                } elseif ($part === 'id') {
                    //it's possible to filter by ID only if it's relationship
                    $type = 'string';
                } else {
                    throw new ExpressionException(
                        Messages::failedToAccessProperty($part, $classMetadata->getClassName())
                    );
                }
            }
            return $this->ex()->field($identifier, $type);
        } catch (MetadataException | ExpressionError $exception) {
            throw new ExpressionException(Messages::syntaxError(), previous: $exception);
        }
    }
}

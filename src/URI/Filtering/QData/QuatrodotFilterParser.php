<?php

/**
 * Created by uzivatel
 * at 22.03.2022 15:03
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering\QData;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use JSONAPI\Expression\Ex;
use JSONAPI\Expression\Exception\ExpressionError;
use JSONAPI\Expression\Exception\IncomparableExpressions;
use JSONAPI\Expression\Exception\InvalidArgument;
use JSONAPI\Expression\Expression;
use JSONAPI\Expression\Literal\ArrayValue;
use JSONAPI\Expression\Type\TBoolean;
use JSONAPI\Expression\Type\TDateTime;
use JSONAPI\Expression\Type\TNumeric;
use JSONAPI\Expression\Type\TString;
use JSONAPI\Mapper\Document\Members;
use JSONAPI\Mapper\Exception\Http\ExpressionException;
use JSONAPI\Mapper\Exception\Metadata\MetadataException;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\URI\Filtering\FilterInterface;
use JSONAPI\Mapper\URI\Filtering\FilterParserInterface;
use JSONAPI\Mapper\URI\Filtering\KeyWord;
use JSONAPI\Mapper\URI\Filtering\Messages;
use JSONAPI\Mapper\URI\Filtering\Parser;

/**
 * Class FilterParser
 *
 * @package JSONAPI\URI\Filtering\Quatrodot
 */
class QuatrodotFilterParser extends Parser implements FilterParserInterface
{
    /**
     * @var QuatrodotResult
     */
    private QuatrodotResult $result;

    /**
     * @inheritDoc
     */
    public function parse(mixed $data, string $className): FilterInterface
    {
        parent::parse($data, $className);
        $this->result = new QuatrodotResult();
        if (is_string($data)) {
            $this->result->setOrigin($data);
            $phrases = explode(KeyWord::PHRASE_SEPARATOR->value, $data);
            $tree    = [];
            foreach ($phrases as $phrase) {
                $tokens = explode(KeyWord::VALUE_SEPARATOR->value, $phrase);
                $field  = array_shift($tokens);
                $op     = array_shift($tokens);
                $args   = $tokens;
                if ($field && $op && $args) {
                    $tree[$field][] = [$field, $op, $args];
                } else {
                    throw new ExpressionException(Messages::syntaxError());
                }
            }
            $condition = $this->parseExpression($tree);
            $this->result->setCondition($condition);
        }
        return $this->result;
    }

    /**
     * @param array<string, array<int, array<int,array<int, string>|string>>> $expression
     *
     * @return Expression
     * @throws ExpressionException
     */
    private function parseExpression(array $expression): mixed
    {
        return $this->parseAnd($expression);
    }

    /**
     * @param array<array<string|array<string>>> $expressionTree
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseAnd(array $expressionTree): mixed
    {
        $left = null;
        foreach ($expressionTree as $expressions) {
            $right = $this->parseOr($expressions);
            if ($left) {
                $left = $this->ex()->and($left, $right);
            } else {
                $left = $right;
            }
        }
        return $left;
    }

    /**
     * @param array<string|array<string>> $expressions
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseOr(array $expressions): mixed
    {
        $left = null;
        foreach ($expressions as $expression) {
            $right = $this->parseComparison($expression);
            if ($left) {
                $left = $this->ex()->or($left, $right);
            } else {
                $left = $right;
            }
        }
        return $left;
    }

    /**
     * @param array<string|array<string>> $expression
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseComparison(array $expression): mixed
    {
        try {
            $field   = array_shift($expression);
            $op      = array_shift($expression);
            $args    = array_shift($expression);
            $left    = $this->parseField($field);
            $operand = KeyWord::tryFrom($op);
            $right   = $this->parseArgs($args, $this->getAttributeForIdentifier($field));
            $ex      = match ($operand) {
                KeyWord::LOGICAL_EQUAL                 => $this->ex()->eq($left, ...$right),
                KeyWord::LOGICAL_NOT_EQUAL             => $this->ex()->ne($left, ...$right),
                KeyWord::LOGICAL_GREATER_THAN          => $this->ex()->gt($left, ...$right),
                KeyWord::LOGICAL_GREATER_THAN_OR_EQUAL => $this->ex()->ge($left, ...$right),
                KeyWord::LOGICAL_LOWER_THAN            => $this->ex()->lt($left, ...$right),
                KeyWord::LOGICAL_LOWER_THAN_OR_EQUAL   => $this->ex()->le($left, ...$right),
                KeyWord::LOGICAL_IN                    => $this->ex()->in($left, new ArrayValue($right)),
                KeyWord::LOGICAL_BETWEEN               => $this->ex()->be($left, ...$right),
                KeyWord::FUNCTION_CONTAINS             => $this->ex()->contains($left, ...$right),
                KeyWord::FUNCTION_STARTS_WITH          => $this->ex()->startsWith($left, ...$right),
                KeyWord::FUNCTION_ENDS_WITH            => $this->ex()->endsWith($left, ...$right),
                default                                => throw new ExpressionException(
                    Messages::operandOrFunctionNotImplemented($operand)
                )
            };
            $this->result->addConditionFor($field, $ex);
            return $ex;
        } catch (InvalidArgument | IncomparableExpressions $exception) {
            throw new ExpressionException($exception->getMessage());
        }
    }

    /**
     * @param string $identifier
     *
     * @return Attribute
     * @throws ExpressionException
     */
    private function getAttributeForIdentifier(string $identifier): Attribute
    {
        try {
            $attribute     = null;
            $classMetadata = $this->getMetadata();
            $parts         = [...explode(".", $identifier)];
            while ($part = array_shift($parts)) {
                if ($classMetadata->hasRelationship($part)) {
                    $classMetadata = $this->getRepository()->getByClass(
                        $classMetadata->getRelationship($part)->target
                    );
                } elseif ($classMetadata->hasAttribute($part) || $part === Members::ID) {
                    $attribute = $classMetadata->getAttribute($part);
                } else {
                    throw new ExpressionException(
                        Messages::failedToAccessProperty($part, $classMetadata->getClassName())
                    );
                }
            }
            return $attribute;
        } catch (MetadataException $exception) {
            throw new ExpressionException(Messages::syntaxError(), previous: $exception);
        }
    }

    /**
     * @param string[]|string[][] $args
     * @param Attribute|null      $attribute
     *
     * @return array<mixed>
     * @throws ExpressionException
     */
    private function parseArgs(mixed $args, ?Attribute $attribute): array
    {
        if (is_null($attribute)) {
            return [$this->ex()->literal($args)];
        } else {
            $type = $attribute->type;
            if ($attribute->type === 'array') {
                $type = $attribute->of;
            }
            $params = [];
            foreach ($args as $arg) {
                $params[] = match ($type) {
                    'int', 'integer'  => $this->parseInt($arg),
                    'bool', 'boolean' => $this->parseBoolean($arg),
                    'float', 'double' => $this->parseFloat($arg),
                    DateTimeInterface::class,
                    DateTimeImmutable::class,
                    DateTime::class   => $this->parseDate($arg),
                    default           => $this->ex()->literal($arg)
                };
            }
            return $params;
        }
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseInt(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_INT)) !== false) {
            return $this->ex()->literal($value);
        }
        throw new ExpressionException(Messages::expressionLexerDigitExpected(0));
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseBoolean(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_BOOL, FILTER_NULL_ON_FAILURE)) !== null) {
            return $this->ex()->literal($value);
        }
        throw new ExpressionException(Messages::expressionLexerBooleanExpected(0));
    }

    /**
     * @param string $arg
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseFloat(string $arg): mixed
    {
        if (($value = filter_var($arg, FILTER_VALIDATE_FLOAT)) !== false) {
            return $this->ex()->literal($value);
        }
        throw new ExpressionException(Messages::expressionLexerDigitExpected(0));
    }

    /**
     * @param string $args
     *
     * @return mixed
     * @throws ExpressionException
     */
    private function parseDate(string $args): mixed
    {
        try {
            $data = new DateTimeImmutable($args);
        } catch (Exception $e) {
            throw new ExpressionException(
                Messages::expressionParserUnrecognizedLiteral('datetime', $args, 0),
                previous: $e
            );
        }
        return $this->ex()->literal($data);
    }
}

<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering;

use JSONAPI\Mapper\Exception\Http\BadRequest;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\URI\Path\PathInterface;

/**
 * Interface FilterParserInterface
 *
 * @package JSONAPI\URI\Filtering
 */
interface FilterParserInterface
{
    /**
     * @param mixed  $data
     * @param string $className primary data className
     *
     * @return FilterInterface
     * @throws BadRequest
     * @throws MetadataNotFound
     */
    public function parse(mixed $data, string $className): FilterInterface;
}

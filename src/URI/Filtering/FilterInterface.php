<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Filtering;

use JSONAPI\Mapper\URI\QueryPartInterface;

/**
 * Interface FilterInterface
 *
 * @package JSONAPI\URI\Filtering
 */
interface FilterInterface extends QueryPartInterface
{
    /**
     * @return mixed
     */
    public function getCondition(): mixed;
}

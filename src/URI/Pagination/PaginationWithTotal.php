<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\URI\Pagination;

/**
 * Interface UseTotalCount
 *
 * @package JSONAPI\URI\Pagination
 */
interface PaginationWithTotal extends PaginationInterface
{
    /**
     * Sets total pages count
     *
     * @param int $total
     *
     * @return void
     */
    public function setTotal(int $total): void;

    /**
     * @param int $limit
     *
     * @return void
     */
    public function setLimit(int $limit): void;
}

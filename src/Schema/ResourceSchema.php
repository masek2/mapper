<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Schema;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Meta;
use JSONAPI\Mapper\Metadata\Relationship;

/**
 * Class ResourceSchema
 *
 * @package JSONAPI\Schema
 */
final class ResourceSchema
{
    /**
     * @var string
     */
    private string $className;
    /**
     * @var string
     */
    private string $type;
    /**
     * @var Id
     */
    private Id $id;
    /**
     * @var Attribute[]
     */
    private array $attributes;
    /**
     * @var Relationship[]
     */
    private array $relationships;
    /**
     * @var Meta|null
     */
    private ?Meta $meta;
    /**
     * @var array<string> methods
     */
    private array $methods;

    /**
     * ResourceSchema constructor.
     *
     * @param string        $className
     * @param Id            $id
     * @param string|null   $type
     * @param array<Attribute>     $attributes
     * @param array<Relationship>  $relationships
     * @param Meta|null     $resourceMeta
     * @param array<string> $methods
     */
    public function __construct(
        string $className,
        Id $id,
        string $type = null,
        array $attributes = [],
        array $relationships = [],
        ?Meta $resourceMeta = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ]
    ) {
        $this->className     = $className;
        $this->type          = $type;
        $this->id            = $id;
        $this->attributes    = $attributes;
        $this->relationships = $relationships;
        $this->meta          = $resourceMeta;
        $this->methods       = $methods;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return $this->relationships;
    }

    /**
     * @return Meta|null
     */
    public function getMeta(): ?Meta
    {
        return $this->meta;
    }

    /**
     * @return array<string>
     */
    public function getMethods(): array
    {
        return $this->methods;
    }
}

<?php

/**
 * Created by tomas
 * at 20.03.2021 21:18
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Exception\Document\AlreadyInUse;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JSONAPI\Mapper\Exception\Driver\DriverException;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;
use JSONAPI\Mapper\URI\Inclusion\InclusionInterface;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Pagination\PaginationInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class RelationshipsProcessor
 *
 * @package JSONAPI\Encoding
 */
class RelationshipsProcessor extends FieldsProcessor implements ResourceProcessor
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Encoder
     */
    private Encoder $encoder;
    /**
     * @var InclusionInterface|null
     */
    private ?InclusionInterface $inclusion;

    /**
     * RelationshipsProcessor constructor.
     *
     * @param Encoder                 $encoder
     * @param MetadataRepository      $repository
     * @param LoggerInterface|null    $logger
     * @param InclusionInterface|null $inclusion
     * @param FieldsetInterface|null  $fieldset
     */
    public function __construct(
        Encoder $encoder,
        MetadataRepository $repository,
        LoggerInterface $logger = null,
        InclusionInterface $inclusion = null,
        FieldsetInterface $fieldset = null
    ) {
        parent::__construct($fieldset);
        $this->encoder    = $encoder;
        $this->repository = $repository;
        $this->logger     = $logger ?? new NullLogger();
        $this->inclusion  = $inclusion;
    }

    /**
     * @param ResourceObject $resource
     * @param object         $object
     *
     * @return void
     * @throws AlreadyInUse
     * @throws ForbiddenCharacter
     * @throws ForbiddenDataType
     * @throws MetadataNotFound
     * @throws DriverException
     */
    public function process(
        ResourceObject $resource,
        object $object
    ): void {
        $metadata = $this->repository->getByType($resource->getType());
        foreach ($metadata->getRelationships() as $field) {
            if ($this->showField($field, $resource)) {
                $relationship = new Relationship($field->name, $resource, $field->withData);
                $relationship->setData($this->getData($field, $object));
                $resource->addRelationship($relationship);
                $this->logger->debug("Adding relationship {$field->name}.");
            }
        }
    }


    /**
     * @param \JSONAPI\Mapper\Metadata\Relationship $field
     * @param mixed                                 $object
     *
     * @return ResourceObjectIdentifier|ResourceCollection|null
     * @throws DriverException
     * @throws MetadataNotFound
     */
    private function getData(
        \JSONAPI\Mapper\Metadata\Relationship $field,
        mixed $object
    ): ResourceObjectIdentifier|ResourceCollection|null {
        if ($field->withData) {
            $value = $field->getter != null ? call_user_func([$object, $field->getter]) : $object->{$field->property};
            if ($field->isCollection) {
                if (!($value instanceof ObjectCollection)) {
                    $value = new ObjectCollection($value);
                }
                $total = $value->count();
                if (is_int($field->withData)) {
                    $value = $value->slice(0, $field->withData);
                }
                $data = new ResourceCollection([], $total);
                foreach ($value as $object) {
                    $data->add($this->encoder->identify($object));
                }
            } elseif ($value) {
                $data = $this->encoder->identify($value);
            } else {
                $data = null;
            }
            return $data;
        } elseif ($field->isCollection) {
            return new ResourceCollection();
        } else {
            return null;
        }
    }
}

<?php

/**
 * Created by tomas
 * at 20.03.2021 16:56
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Metadata\Field;
use JSONAPI\Mapper\URI\Fieldset\FieldsetInterface;

/**
 * Class FieldsProcessor
 *
 * @package JSONAPI\Encoding
 */
abstract class FieldsProcessor
{
    private ?FieldsetInterface $fieldset;

    /**
     * FieldsProcessor constructor.
     *
     * @param FieldsetInterface|null $fieldset
     */
    public function __construct(?FieldsetInterface $fieldset)
    {
        $this->fieldset = $fieldset;
    }


    /**
     * @param Field  $field
     * @param object $object
     *
     * @return mixed
     */
    protected function getValue(Field $field, object $object): mixed
    {
        return $field->getter != null ? call_user_func([$object, $field->getter]) : $object->{$field->property};
    }

    /**
     * @param Field          $field
     * @param ResourceObject $resource
     *
     * @return bool
     */
    protected function showField(Field $field, ResourceObject $resource): bool
    {
        return $this->fieldset?->showField($resource->getType(), $field->name) ?? true;
    }
}

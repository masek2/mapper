<?php

/**
 * Created by tomas
 * at 20.03.2021 15:36
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Exception\JsonApiException;

/**
 * Interface Processor
 *
 * @package JSONAPI\Encoding
 * @method void process()
 */
interface Processor
{
}

<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:19
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Exception\Driver\ProcessorException;
use JSONAPI\Mapper\Exception\JsonApiException;

interface ResourceIdentifierProcessor extends Processor
{
    /**
     * @param ResourceObjectIdentifier $resource
     * @param object                                  $object
     *
     * @return void
     * @throws ProcessorException
     */
    public function process(ResourceObjectIdentifier $resource, object $object): void;
}

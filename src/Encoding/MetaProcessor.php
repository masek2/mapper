<?php

/**
 * Created by tomas
 * at 20.03.2021 20:34
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\Exception\Metadata\RelationNotFound;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class MetaProcessor
 *
 * @package JSONAPI\Encoding
 */
class MetaProcessor implements ResourceIdentifierProcessor, ResourceProcessor, RelationshipProcessor
{
    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;
    /**
     * @var LoggerInterface
     */
    private readonly LoggerInterface $logger;

    /**
     * MetaProcessor constructor.
     *
     * @param MetadataRepository   $repository
     * @param LoggerInterface|null $logger
     */
    public function __construct(MetadataRepository $repository, LoggerInterface $logger = null)
    {
        $this->repository = $repository;
        $this->logger     = $logger ?? new NullLogger();
    }

    /**
     * @inheritDoc
     */
    public function process(
        ResourceObjectIdentifier|ResourceObject|Relationship $resource,
        object $object
    ): void {
        if ($resource instanceof ResourceObjectIdentifier) {
            $metadata = $this->repository->getByType($resource->getType());
            if ($meta = $metadata->getMeta()) {
                $this->logger->debug("Found meta for {$resource->getType()}");
                $meta = call_user_func([$object, $meta->getter]);
                $resource->setMeta($meta);
            }
        }
        if ($resource instanceof Relationship) {
            $metadata = $this->repository->getByType($resource->getParent()->getType());
            if ($metadata->getRelationship($resource->getKey())->meta) {
                $this->logger->debug("Found meta for relationship {$resource->getKey()}");
                $meta = call_user_func([$object, $metadata->getRelationship($resource->getKey())->meta->getter]);
                $resource->setMeta($meta);
            }
        }
    }
}

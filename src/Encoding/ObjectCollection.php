<?php

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use ArrayIterator;
use Closure;
use Countable;
use IteratorAggregate;
use JSONAPI\Mapper\Exception\Data\CollectionException;
use ReflectionException;
use ReflectionMethod;
use ReflectionObject;
use ReflectionProperty;
use Traversable;

use function count;

/**
 * Class DataCollection
 *
 * @package JSONAPI\Mapper\Encoding
 * @template T of object
 * @implements IteratorAggregate<T>
 */
class ObjectCollection implements Countable, IteratorAggregate
{
    public const SORT_DESC = 'DESC';
    public const SORT_ASC = 'ASC';

    /**
     * @var array<T> data
     */
    private array $data = [];
    /**
     * @var int|null total
     */
    private ?int $total;

    /**
     * @param array<mixed> $data
     * @param int|null $total
     */
    public function __construct(array $data = [], int $total = null)
    {
        $this->total = $total;
        foreach ($data as $item) {
            $this->add($item);
        }
    }

    /**
     * @param T $item
     *
     * @return void
     */
    public function add(object $item): void
    {
        $this->data[] = $item;
    }

    /**
     * @param int $offset
     * @param int|null $length
     *
     * @return ObjectCollection<object>
     */
    public function slice(int $offset, int $length = null): ObjectCollection
    {
        return new ObjectCollection(array_slice($this->data, $offset, $length), $this->total);
    }

    /**
     * @param Closure $callback
     *
     * @return ObjectCollection<object>
     */
    public function filter(Closure $callback): ObjectCollection
    {
        return new ObjectCollection(array_filter($this->data, $callback, ARRAY_FILTER_USE_BOTH), $this->total);
    }

    /**
     * Returns Collection as array
     *
     * @return array<T>
     */
    public function values(): array
    {
        return array_values($this->data);
    }

    /**
     * @param T $item
     *
     * @return bool
     */
    public function has(object $item): bool
    {
        return in_array($item, $this->data, true);
    }

    /**
     * @param T $item
     *
     * @return bool
     */
    public function remove(object $item): bool
    {
        $key = array_search($item, $this->data, true);
        if ($key === false) {
            return false;
        }
        unset($this->data[$key]);
        return true;
    }

    /**
     * Sort function for object collection
     *
     * @param array<string, string> $order
     *
     * @return ObjectCollection<T>
     * @throws CollectionException
     */
    public function sort(array $order): self
    {
        $next = static function (): int {
            return 0;
        };
        foreach (array_reverse($order) as $field => $ordering) {
            $orientation = $ordering === self::SORT_DESC ? -1 : 1;
            $next = static function ($a, $b) use ($field, $next, $orientation): int {
                $accessor = function (object $object, string $field) {
                    $fields = explode('.', $field);
                    $value = $object;
                    foreach ($fields as $field) {
                        $found = false;
                        $ref = new ReflectionObject($object);
                        if (
                            !empty(array_filter(
                                $ref->getProperties(ReflectionProperty::IS_PUBLIC),
                                fn(ReflectionProperty $property) => $property->getName() === $field
                            ))
                        ) {
                            $value = $object->{$field};
                            $found = true;
                        } elseif (
                            $methods = array_filter(
                                $ref->getMethods(ReflectionMethod::IS_PUBLIC),
                                fn(ReflectionMethod $method) => strtolower(str_replace(
                                    ['get', 'is'],
                                    '',
                                    $method->getName()
                                )) === $field
                            )
                        ) {
                            $method = array_pop($methods);
                            $value = $method->invoke($object);
                            $found = true;
                        }
                        if (!$found) {
                            throw new CollectionException(
                                "Property $field on " . get_class($value) . " not found.",
                                510
                            );
                        }
                    }
                    return $value;
                };
                $aValue = $accessor($a, $field);
                $bValue = $accessor($b, $field);
                if ($aValue === $bValue) {
                    return $next($a, $b);
                }
                return ($aValue > $bValue ? 1 : -1) * $orientation;
            };
        }
        uasort($this->data, $next);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return $this->total ?? count($this->data);
    }
}

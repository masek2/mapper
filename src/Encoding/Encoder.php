<?php

/**
 * Created by tomas
 * at 20.03.2021 15:32
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Id;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Document\ResourceObjectIdentifier;
use JSONAPI\Mapper\Document\Type;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JSONAPI\Mapper\Exception\Driver\DriverException;
use JSONAPI\Mapper\Exception\Driver\ProcessorException;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\Helper\DoctrineProxyTrait;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Encoder
 *
 * @package JSONAPI\Encoding
 */
class Encoder
{
    use DoctrineProxyTrait;

    /**
     * @var MetadataRepository
     */
    private MetadataRepository $repository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ObjectCollection<Processor>
     */
    private ObjectCollection $processors;

    /**
     * Encoder constructor.
     *
     * @param MetadataRepository   $metadataRepository
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        MetadataRepository $metadataRepository,
        LoggerInterface $logger = null
    ) {
        $this->repository = $metadataRepository;
        $this->logger     = $logger ?? new NullLogger();
        $this->processors = new ObjectCollection();
    }

    /**
     * @param Processor $processor
     *
     * @return void
     */
    public function addProcessor(Processor $processor): void
    {
        $this->processors->add($processor);
    }

    /**
     * @param Processor $processor
     *
     * @return void
     */
    public function removeProcessor(Processor $processor): void
    {
        $this->processors->remove($processor);
    }

    /**
     * @param object $object
     *
     * @return ResourceObject
     * @throws MetadataNotFound
     * @throws ProcessorException
     */
    public function encode(object $object): ResourceObject
    {
        list($type, $id) = $this->getTypeAndId($object);
        $resource = new ResourceObject($type, $id);
        foreach ($this->processors as $processor) {
            if ($processor instanceof ResourceProcessor) {
                $processor->process($resource, $object);
            }
        }
        foreach ($resource->getRelationships() as $relationship) {
            foreach ($this->processors as $processor) {
                if ($processor instanceof RelationshipProcessor) {
                    $processor->process($relationship, $object);
                }
            }
        }
        return $resource;
    }

    /**
     * @param object $object
     *
     * @return ResourceObjectIdentifier
     * @throws MetadataNotFound
     * @throws DriverException
     */
    public function identify(object $object): ResourceObjectIdentifier
    {
        list($type, $id) = $this->getTypeAndId($object);
        $resource = new ResourceObjectIdentifier($type, $id);
        foreach ($this->processors as $processor) {
            if ($processor instanceof ResourceIdentifierProcessor) {
                $processor->process($resource, $object);
            }
        }
        return $resource;
    }

    /**
     * @param object $object
     *
     * @return array{Type, Id}
     * @throws MetadataNotFound
     */
    private function getTypeAndId(object $object): array
    {
        $id        = null;
        $type      = null;
        $className = Encoder::clearDoctrineProxyPrefix(get_class($object));
        $metadata  = $this->repository->getByClass($className);
        try {
            $type = new Type($metadata->getType());
            if ($metadata->getId()->property != null) {
                $value = $object->{$metadata->getId()->property};
            } else {
                $value = (string)call_user_func([$object, $metadata->getId()->getter]);
            }
            $id = new Id($value);
        } catch (ForbiddenCharacter | ForbiddenDataType $e) {
            $this->logger->alert($e->getMessage(), $e->getTrace());
        }
        return [$type, $id];
    }
}

<?php

/**
 * Created by tomas
 * at 20.03.2021 21:05
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Link;
use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Document\ResourceCollection;
use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Exception\Document\ForbiddenCharacter;
use JSONAPI\Mapper\Exception\Document\ForbiddenDataType;
use JSONAPI\Mapper\Exception\Metadata\MetadataNotFound;
use JSONAPI\Mapper\URI\LinkFactory;
use JSONAPI\Mapper\URI\Pagination\LimitOffsetPagination;
use JSONAPI\Mapper\URI\Pagination\PagePagination;
use JSONAPI\Mapper\URI\Pagination\PaginationInterface;
use JSONAPI\Mapper\URI\Pagination\PaginationWithTotal;

use function Symfony\Component\String\s;

/**
 * Class LinksProcessor
 *
 * @package JSONAPI\Encoding
 */
class LinksProcessor implements ResourceProcessor, RelationshipProcessor
{
    /**
     * @var LinkFactory linkFactory
     */
    private LinkFactory $linkFactory;
    /**
     * @var string baseURL
     */
    private string $baseURL;
    /**
     * @var PaginationInterface|null pagination
     */
    private ?PaginationInterface $pagination;

    /**
     * LinksProcessor constructor.
     *
     * @param LinkFactory              $linkFactory
     * @param string                   $baseURL
     * @param PaginationInterface|null $pagination
     */
    public function __construct(LinkFactory $linkFactory, string $baseURL, PaginationInterface $pagination = null)
    {
        $this->linkFactory = $linkFactory;
        $this->baseURL     = $baseURL;
        $this->pagination  = $pagination;
    }

    /**
     * @inheritDoc
     */
    public function process(
        ResourceObject|Relationship $resource,
        object $object
    ): void {
        if ($resource instanceof ResourceObject) {
            $links = $this->linkFactory->getResourceLinksByType($resource->getType());
            $self  = s($links[Link::SELF])
                ->prepend($this->baseURL)
                ->replace('{id}', rawurlencode($resource->getId()))
                ->toString();
            $resource->getLinks()->create(Link::SELF, $self);
        }
        if ($resource instanceof Relationship) {
            $links = $this->linkFactory->getResourceLinksByType($resource->getParent()->getType());
            $self  = s($links[$resource->getKey()][Link::SELF])
                ->prepend($this->baseURL)
                ->replace('{id}', rawurlencode($resource->getParent()->getId()))
                ->toString();
            $resource->getLinks()->create(Link::SELF, $self);
            $related = s($links[$resource->getKey()][Link::RELATED])
                ->prepend($this->baseURL)
                ->replace('{id}', rawurlencode($resource->getParent()->getId()))
                ->toString();
            $resource->getLinks()->create(Link::RELATED, $related);
            if (
                $resource->getData() instanceof ResourceCollection
                && $this->pagination instanceof PaginationWithTotal
                && is_int($resource->getWithData())
            ) {
                $className  = $this->pagination::class;
                /** @var PaginationWithTotal $pagination */
                $pagination = new $className();
                $pagination->setLimit($resource->getWithData());
                $pagination->setTotal($resource->getData()->count());
                $resource->getLinks()->create(Link::FIRST, $self . '?' . $pagination->first());
                if ($pagination->prev()) {
                    $resource->getLinks()->create(Link::PREV, $self . '?' . $pagination->prev());
                }
                if ($pagination->next()) {
                    $resource->getLinks()->create(Link::NEXT, $self . '?' . $pagination->next());
                }
                if ($pagination->last()) {
                    $resource->getLinks()->create(Link::LAST, $self . '?' . $pagination->last());
                }
            }
        }
    }
}

<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:21
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\Relationship;
use JSONAPI\Mapper\Exception\Driver\ProcessorException;

interface RelationshipProcessor extends Processor
{
    /**
     * @param Relationship $resource
     * @param object                                  $object
     *
     * @return void
     * @throws ProcessorException
     */
    public function process(Relationship $resource, object $object): void;
}

<?php

/**
 * Created by uzivatel
 * at 22.06.2022 15:19
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Encoding;

use JSONAPI\Mapper\Document\ResourceObject;
use JSONAPI\Mapper\Exception\Driver\ProcessorException;

interface ResourceProcessor extends Processor
{
    /**
     * @param ResourceObject $resource
     * @param object                                  $object
     *
     * @return void
     * @throws ProcessorException
     */
    public function process(ResourceObject $resource, object $object): void;
}

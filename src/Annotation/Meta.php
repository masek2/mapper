<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

/**
 * Class Meta
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final class Meta extends \JSONAPI\Mapper\Metadata\Meta
{
    /**
     * @inheritDoc
     */
    public function __construct(string $getter)
    {
        parent::__construct($getter);
    }
}

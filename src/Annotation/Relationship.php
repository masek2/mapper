<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Metadata\Meta;

/**
 * Class Relationship
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
final class Relationship extends \JSONAPI\Mapper\Metadata\Relationship
{
    /**
     * @var array<string> methods
     */
    public array $methods = [
        RequestMethodInterface::METHOD_GET,
        RequestMethodInterface::METHOD_POST,
        RequestMethodInterface::METHOD_PATCH,
        RequestMethodInterface::METHOD_DELETE
    ];

    /**
     * @inheritDoc
     */
    public function __construct(
        string $target,
        string $name = null,
        string $property = null,
        string $getter = null,
        string $setter = null,
        ?bool $isCollection = null,
        ?bool $nullable = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ],
        int|bool $withData = 25
    ) {
        parent::__construct(
            $target,
            $name,
            $property,
            $getter,
            $setter,
            $isCollection,
            $this->meta,
            $nullable,
            $methods,
            $withData
        );
    }
}

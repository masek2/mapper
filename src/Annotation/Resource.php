<?php

/**
 * Created by tomas.benedikt@gmail.com
 */

declare(strict_types=1);

namespace JSONAPI\Mapper\Annotation;

use Fig\Http\Message\RequestMethodInterface;

/**
 * Class Resource
 *
 * @package JSONAPI\Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Resource
{
    /**
     * @var Meta|null
     */
    public ?Meta $meta = null;
    /**
     * @var string|null type
     */
    public ?string $type = null;
    /**
     * @var array<string> methods
     */
    public array $methods = [
        RequestMethodInterface::METHOD_GET,
        RequestMethodInterface::METHOD_POST,
        RequestMethodInterface::METHOD_PATCH,
        RequestMethodInterface::METHOD_DELETE
    ];

    /**
     * Resource constructor.
     *
     * @param string|null $type
     * @param array<string>       $methods
     */
    public function __construct(
        ?string $type = null,
        array $methods = [
            RequestMethodInterface::METHOD_GET,
            RequestMethodInterface::METHOD_POST,
            RequestMethodInterface::METHOD_PATCH,
            RequestMethodInterface::METHOD_DELETE
        ]
    ) {
        $this->type     = $type;
        $this->methods  = $methods;
    }
}

image: registry.gitlab.com/jaspr/mapper:8.1

stages:
    - build
    - test
    - analyse
    - release
    - docs

variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"

build:
    stage: build
    script:
        - composer -q install
        - composer validate --no-check-all --strict
    artifacts:
        paths:
            - vendor

test:unit:
    stage: test
    coverage: '/^\s*Lines:\s*\d+.\d+\%/'
    script:
        - vendor/bin/phpunit --coverage-text --colors=never
    artifacts:
        paths:
            - tmp
        expire_in: 1h

code_sniffer:
    stage: analyse
    script:
        - vendor/bin/phpcs

sonar:
    stage: analyse
    only:
        - 'master'
    except:
        - merge_requests
    image:
        name: sonarsource/sonar-scanner-cli:latest
        entrypoint: [ "" ]
    cache:
        key: "${CI_JOB_NAME}"
        paths:
            - .sonar/cache
    script:
        - sonar-scanner
    dependencies:
        - test:unit

phpstan:
    stage: analyse
    script:
        - vendor/bin/phpstan -n --no-ansi analyse

pages:
    stage: docs
    only:
        - tags
    image:
        name: phpdoc/phpdoc:3
        entrypoint: [ "" ]
    services: [ ]
    script:
        - phpdoc -d src -t public
    artifacts:
        paths:
            - public
        expire_in: 1h

release:
    image: registry.gitlab.com/gitlab-org/release-cli
    stage: release
    rules:
        -   if: '$CI_COMMIT_TAG'
    script:
        - echo "Creating a release for version $CI_COMMIT_TAG"
    release:
        name: "$CI_COMMIT_TAG"
        description: "See [changelog](CHANGELOG.md)"
        tag_name: '$CI_COMMIT_TAG'
        ref: '$CI_COMMIT_TAG'
